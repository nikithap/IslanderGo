package com.tamucc.navigation.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tamucc.navigation.model.User;

@Repository
public class UserDao extends GenericDaoJpaImpl<User, Integer> {

	public User getUser(String email, String password) {

		List<User> users = entityManager
				.createQuery("SELECT u FROM User u WHERE u.email = :email and u.password=:password")
				.setParameter("email", email).setParameter("password", password).getResultList();

		System.out.println(email);
		System.out.println(password);
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}

	public User getUserbyEmail(String email) {
		List<User> users = entityManager.createQuery("SELECT u FROM User u where u.email=:email")
				.setParameter("email", email).getResultList();
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}

	public User getUserbyId(String uid) {
		List<User> users = entityManager.createQuery("SELECT u FROM User u where u.uid=:uid").setParameter("uid", uid)
				.getResultList();
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}

}
