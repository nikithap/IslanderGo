package com.tamucc.navigation.dao;

import org.springframework.stereotype.Repository;

import com.tamucc.navigation.model.Event;

@Repository
public class EventDao extends GenericDaoJpaImpl<Event, Integer> {

}
