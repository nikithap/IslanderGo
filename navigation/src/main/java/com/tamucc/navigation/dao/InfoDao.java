package com.tamucc.navigation.dao;

import org.springframework.stereotype.Repository;

import com.tamucc.navigation.model.Info;

@Repository
public class InfoDao extends GenericDaoJpaImpl<Info, Integer> {

}
