package com.tamucc.navigation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tamucc.navigation.commons.EncryptPassword;
import com.tamucc.navigation.dao.UserDao;
import com.tamucc.navigation.model.User;

@Service("userService")

public class UserService {

	@Autowired
	UserDao userDao;

	@Transactional
	public User registerUser(User user) {

		user.setPassword(EncryptPassword.generateHash(user.getPassword()));
		if (getUserbyEmail(user.getEmail()) == null) {
			return userDao.create(user);
		} else {

			user.setError(true);
			user.setErrormessage("Email already exists");
			return user;
		}

	}

	public User isUserValid(String email, String password) {
		password = EncryptPassword.generateHash(password);

		User user = userDao.getUser(email, password);
		if (user == null) {
			user = new User();
			user.setError(true);
			user.setErrormessage("Invalid credentials");
			return user;
		} else {
			return user;
		}
	}

	public User getUserbyEmail(String email) {

		return userDao.getUserbyEmail(email);

	}

	public User getUserbyId(String uid) {

		return userDao.getUserbyId(uid);

	}

}
