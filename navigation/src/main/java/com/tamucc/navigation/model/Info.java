package com.tamucc.navigation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "info")
public class Info {

	@Id
	@Column(name = "id")
	private int id;

	private String information;

	@Column(name = "buildingid")

	private int buildingname;

	@Transient
	private String building;

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public int getBuildingname() {
		return buildingname;
	}

	public void setBuildingname(int buildingname) {
		this.buildingname = buildingname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

}
