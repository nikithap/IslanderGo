package com.tamucc.navigation.model;

import java.util.HashMap;
import java.util.Map;

public class BuildingMap {

	public static Map<Integer, String> buildingMap = new HashMap<>();

	static {

		buildingMap.put(1, "Mary and Jeff Bell Library");
		buildingMap.put(2, "Starfish Parking Lot");
		buildingMap.put(3, "Center for the Sciences");
		buildingMap.put(4, "Engineering Building");
		buildingMap.put(5, "Glasscock Center");
		buildingMap.put(6, "Bayside Parking Garage");
		buildingMap.put(7, "Seahorse Parking Lot");
		buildingMap.put(8, "Jellyfish Parking Lot");
		buildingMap.put(9, "Center for the Arts");
		buildingMap.put(10, "University Services Center");
		buildingMap.put(11, "Corpus Christi Hall");
		buildingMap.put(12, "Early Childhood Development Center");
		buildingMap.put(13, "Michael and Karen OConnor Building");
		buildingMap.put(14, "Student Services Center");
		buildingMap.put(15, "Bay Hall");
		buildingMap.put(16, "Faculty Center");
		buildingMap.put(17, "Carlos Truan Natural Resources Center");
		buildingMap.put(18, "Harte Research Institute");
		buildingMap.put(19, "NRC Lot");
		buildingMap.put(20, "HRI Lot");
		buildingMap.put(21, "Conrad Blucher Institute for Surveying/Science");
		buildingMap.put(22, "Science Lab");
		buildingMap.put(23, "Physical Plant");
		buildingMap.put(24, "Purchasing");
		buildingMap.put(25, "Angelfish Parking Lot");
		buildingMap.put(26, "Classroom West");
		buildingMap.put(27, "University Center");
		buildingMap.put(28, "Dugan Wellness Center");
		buildingMap.put(29, "Island Hall");
		buildingMap.put(30, "Turtle Cove Parking Lot");
		buildingMap.put(31, "Field House");
		buildingMap.put(32, "Classroom East");
		buildingMap.put(33, "Dining Hall");
		buildingMap.put(34, "Center for Instruction");
		buildingMap.put(35, "Driftwood");
		buildingMap.put(36, "Sandpiper");
		buildingMap.put(37, "Hammerhead Parking Lot");
		buildingMap.put(38, "Sanddollar Parking Lot");
		buildingMap.put(39, "Seabreeze Parking Lot");
		buildingMap.put(40, "Tarpon Parking Lot");
		buildingMap.put(41, "Curlew Parking Lot");
		buildingMap.put(42, "Performing Arts Ceter");

	}
}
