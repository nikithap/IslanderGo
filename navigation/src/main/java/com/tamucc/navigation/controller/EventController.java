package com.tamucc.navigation.controller;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tamucc.navigation.dao.EventDao;
import com.tamucc.navigation.model.Event;

@Controller
public class EventController {

	@Autowired

	EventDao eventDao;

	@Transactional
	@RequestMapping(value = "addEvent", method = RequestMethod.POST)
	public ModelAndView addEvent(@RequestParam Map<String, String> event) {

		final ObjectMapper mapper = new ObjectMapper(); // jackson's
														// objectmapper
		final Event pojo = mapper.convertValue(event, Event.class);
		eventDao.create(pojo);

		ModelAndView modelAndView = allEvents();
		modelAndView.addObject("message", "Event has been added");
		return modelAndView;

	}
	
	@RequestMapping(value = "allEvents", method = RequestMethod.GET)
	public @ResponseBody ModelAndView allEvents() {

		ModelAndView result = new ModelAndView("eventslist");
		result.addObject("data", eventDao.findAll());

		return result;

	}

	@RequestMapping(value="/", method = RequestMethod.GET)
	public String welcome() {
		return "addEvent";
	}

}
