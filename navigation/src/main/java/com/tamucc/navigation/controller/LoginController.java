package com.tamucc.navigation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tamucc.navigation.model.User;
import com.tamucc.navigation.service.UserService;

@Controller
public class LoginController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public @ResponseBody User isUserValid(@RequestBody User user) {

		return userService.isUserValid(user.getEmail(), user.getPassword());

	}
	
	
}
