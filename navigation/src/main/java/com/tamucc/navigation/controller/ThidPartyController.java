package com.tamucc.navigation.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.oracle.jrockit.jfr.EventInfo;
import com.tamucc.navigation.dao.EventDao;
import com.tamucc.navigation.dao.InfoDao;
import com.tamucc.navigation.model.BuildingMap;
import com.tamucc.navigation.model.Event;
import com.tamucc.navigation.model.EventsAndInfos;
import com.tamucc.navigation.model.Info;
import com.tamucc.navigation.model.User;

@Controller
public class ThidPartyController {

	@Value("${direction.url}")
	private String directionUrl;

	@Value("${post.url}")
	private String postUrl;

	@Autowired
	InfoDao infoDao;

	@Autowired
	EventDao eventDao;

	@RequestMapping(value = "direction", method = RequestMethod.GET)
	public ResponseEntity<String> direction(@RequestParam(value = "olat") String olat,
			@RequestParam(value = "olng") String olng, @RequestParam(value = "dlat") String dlat,
			@RequestParam(value = "dlng") String dlng) {

		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = directionUrl + "?" + "olat=" + olat + "&olng=" + olng + "&dlat=" + dlat + "&dlng="
				+ dlng;
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl + "/1", String.class);
		return response;
	}

	@RequestMapping(value = "post", method = RequestMethod.POST)
	public ResponseEntity<String> post(@RequestBody User user) {

		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = postUrl + "";
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl + "/1", String.class);
		return response;

	}

	@RequestMapping(value = "eventsandinfos", method = RequestMethod.POST)
	public @ResponseBody EventsAndInfos getEventsAndInfo() {

		BuildingMap buildingMap = new BuildingMap();
		EventsAndInfos eventsAndInfos = new EventsAndInfos();
		List<Event> eventsList = eventDao.findAll();
		for (Event event : eventsList) {
			event.setBuildingname(buildingMap.buildingMap.get(Integer.parseInt( event.getBuildingname())));
		}
		eventsAndInfos.setEvents(eventsList);
		
		List<Info> infoList = infoDao.findAll();
		
		for (Info info : infoList) {
			info.setBuilding(buildingMap.buildingMap.get( info.getBuildingname()));
		}
		eventsAndInfos.setInfo(infoList);
		return eventsAndInfos;
	}

}
