package com.tamucc.navigation.controller;

import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tamucc.navigation.dao.InfoDao;
import com.tamucc.navigation.model.Event;
import com.tamucc.navigation.model.Info;

@Controller
public class InfoController {

	@Autowired
	InfoDao infoDao;

	@Transactional
	@RequestMapping(value = "addinfo", method = RequestMethod.POST)
	public ModelAndView addEvent(@RequestParam Map<String, String> event) {

		final ObjectMapper mapper = new ObjectMapper(); // jackson's
														// objectmapper
		final Info pojo = mapper.convertValue(event, Info.class);
		infoDao.create(pojo);

		ModelAndView modelAndView = allEvents();
		modelAndView.addObject("message", "Info has been added");
		return modelAndView;

	}

	@RequestMapping(value = "allinfo", method = RequestMethod.GET)
	public @ResponseBody ModelAndView allEvents() {

		ModelAndView result = new ModelAndView("infolist");
		result.addObject("data", infoDao.findAll());

		return result;

	}


	@RequestMapping(value = "addinfo", method = RequestMethod.GET)
	public String welcome() {
		return "addInfo";
	}

	
}
