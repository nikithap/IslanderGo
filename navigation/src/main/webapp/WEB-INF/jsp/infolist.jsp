<%@page import="com.tamucc.navigation.model.BuildingMap"%>
<%@page
	import="org.springframework.boot.autoconfigure.info.ProjectInfoProperties.Build"%>
<%@page import="com.tamucc.navigation.model.Info"%>
<%@page import="com.tamucc.navigation.model.Event"%>
<%@page import="java.util.List"%>
<%
	response.addHeader("Cache-Control",
			"no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
	response.addHeader("Pragma", "no-cache");
	response.addDateHeader("Expires", 0);
%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="header.jsp" />





<body class="nav-md">

	<div class="container body">


		<div class="main_container">

			<jsp:include page="menu.jsp" />

			<div class="right_col" role="main">

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="col-md-8 col-sm-8 col-md-offset-2 col-xs-12 clearfix"
							style="min-height: 550px; background: white;">

							<%
								if (request.getAttribute("message") != null) {
							%>
							<div class="alert alert-info">
								<strong>Info!</strong>
								<%
									out.print(request.getAttribute("message"));
								%>
								<%
									}
								%>

								<table class="table table-striped projects ">
									<thead>
										<tr>
											<th style="width: 1%">#</th>
											<th style="width: 20%">Building Name</th>
											<th>Information</th>
											<th>Edit</th>
											<th>Delete</th>

										</tr>
									</thead>
									<tbody>

										<%
											List<Info> infos = (List<Info>) request.getAttribute("data");
											for (Info event : infos) {
										%>
										<tr>
											<td>#</td>


											<td class="project_progress">
												<%
													out.print(BuildingMap.buildingMap.get(event.getBuildingname()));
												%>
											</td>
											<td>
												<%
													out.print(event.getInformation());
												%>
											</td>


											<td><p data-placement="top" data-toggle="tooltip"
													title="Edit">
													<button class="btn btn-primary btn-xs" data-title="Edit"
														data-toggle="modal" data-target="#edit">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</p></td>
											<td><p data-placement="top" data-toggle="tooltip"
													title="Delete">
													<button class="btn btn-danger btn-xs" data-title="Delete"
														data-toggle="modal" data-target="#delete">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</p></td>


										</tr>
										<%
											}
										%>

									</tbody>
								</table>

							</div>
							<div class="clearfix" style="height: 150px;"></div>
						</div>
					</div>

				</div>
				<br />

			</div>
		</div>

	</div>
</body>