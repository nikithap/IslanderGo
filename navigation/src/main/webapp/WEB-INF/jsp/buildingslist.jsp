
<%@page import="java.util.Map"%>
<%@page import="com.tamucc.navigation.model.BuildingMap"%>
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Select
		Building</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select class="form-control" name="buildingname">

			<%
				for (Map.Entry<Integer, String> entry : BuildingMap.buildingMap.entrySet()) {
			%>
			<option class="" value="<%out.print(entry.getKey());%>">
				<%
					out.print(entry.getValue());
				%>
			</option>
			<%
				}
			%>

		</select>
	</div>
</div>
