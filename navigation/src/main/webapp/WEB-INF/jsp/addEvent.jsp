
<%
	response.addHeader("Cache-Control",
			"no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
	response.addHeader("Pragma", "no-cache");
	response.addDateHeader("Expires", 0);
%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="header.jsp" />





<body class="nav-md">

	<div class="container body">


		<div class="main_container">

			<jsp:include page="menu.jsp" />

			<div class="right_col" role="main">

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="dashboard_graph">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="x_panel">
										<div class="x_title">
											<h2>Add Event</h2>

											<div class="clearfix"></div>
										</div>
										<img src="img/health1.jpg" class="col-md-12 hide" alt="Home"
											style="height: 120px;">

										<div class="x_content">

											<form data-parsley-validate modelAttribute="employee"
												class="form-horizontal form-label-left"
												action="addEvent" method="post">


												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12">Event
														Name <span class="required">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input type="text" name="eventname" required="required"
															class="form-control col-md-7 col-xs-12">
													</div>
												</div>
												<jsp:include page="buildingslist.jsp" />



												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12">Start
														Date <span class="required">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input type="datetime-local" name="startdate" required="required"
															class="form-control col-md-7 col-xs-12">
													</div>
												</div>



												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12">End
														Date <span class="required">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input type="datetime-local" name="enddate" required="required"
															class="form-control col-md-7 col-xs-12">
													</div>
												</div>


												<div class="ln_solid"></div>
												<div class="form-group">
													<div class="btn nextBtn pull-right">
														<button type="submit" class="btn btn-success">Submit</button>
													</div>
												</div>



											</form>
										</div>
									</div>
								</div>
							</div>

						</div>

						<div class="clearfix" style="height: 150px;"></div>
					</div>
				</div>

			</div>
			<br />

		</div>
	</div>

	</div>
</body>