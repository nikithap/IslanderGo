package com.tamucc.navigation;


import java.util.ArrayList;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.tamucc.navigation.database.DB_Building;
import com.tamucc.navigation.database.DB_Operations;
import com.tamucc.navigation.database.DB_Route;

public class RouteActivity extends BaseActivity {

    private ArrayList<DB_Route> routeList;
    private ListView LV;

    Map<String, DB_Building> buildingMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        setTitle("Your Records");
        readRoutesInfoFromDatabase();
        populateLV();

    }

    private void populateLV() {
        ArrayAdapter<DB_Route> adapter = new MyListAdapter();
        LV = findViewById(R.id.routeLV);
        LV.setAdapter(adapter);
        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> lv, View view,
                                    int position, long id) {
                //....click item event
                String fileName = LV.getItemAtPosition(position)
                        .toString();

                broadcastMsg(fileName);
                finish();
            }
        });
    }

    private void broadcastMsg(String bn) {
        sendMessageToHomeActivity("RouteActivity", bn);
    }


    private void sendMessageToHomeActivity(String activity, String bn) {
        Intent intent = new Intent("GetDirection");
        intent.putExtra("Activity", activity);
        intent.putExtra("FileName", bn);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private class MyListAdapter extends ArrayAdapter<DB_Route> {
        public MyListAdapter() {
            super(RouteActivity.this, R.layout.activity_route_listviewitem,
                    routeList);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(
                        R.layout.activity_route_listviewitem, parent, false);
            }

            // Find route to work with
            DB_Route currentRoute = routeList.get(position);

            // Fill the route name
//            TextView nameText = itemView
//                    .findViewById(R.id.route_item_name);
//            nameText.setText(currentRoute.getRid() + ".to:");

            //route destination
            TextView destinationText = itemView
                    .findViewById(R.id.route_item_destination);
            destinationText.setText(currentRoute.getDestination());

            TextView taketime = itemView
                    .findViewById(R.id.taketime);

            taketime.setText(currentRoute.getTakeTime() + " secons");

            //route distance and time
            TextView distanceText = itemView
                    .findViewById(R.id.route_item_distance);
            distanceText.setText(currentRoute.getDistance() + "meters  ");

            ImageView imgRider = itemView.findViewById(R.id.imgRider);
            imgRider.setImageResource(buildingMap.get(currentRoute.getDestination()).getBuildingIcon());


            return itemView;

        }

    }


    private void readRoutesInfoFromDatabase() {
        DB_Operations datasource = new DB_Operations(this);
        datasource.open();
        routeList = datasource.getRouteInfo(true);

        buildingMap = datasource.getBuilding();

        //setListAdapter(new ArrayAdapter<String>(RoutesActivity.this,
        //		android.R.layout.simple_list_item_1, datasource.getRouteInfo()));
        datasource.close();
    }


}