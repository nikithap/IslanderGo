package com.tamucc.navigation.location;

import android.os.AsyncTask;

public class CurrentLocationAsync extends AsyncTask<Void, Integer, String> {

	private CurrentLocation ml;

	
	public CurrentLocationAsync(CurrentLocation ml, String destination) {
		this.ml = ml;
		this.ml.setDestination(destination);
	}

	protected void onPreExecute(String b) {
	}

	@Override
	protected String doInBackground(Void... arg0) {
		ml.beginRoute();
		return null;
	}
	protected void onProgressUpdated(Integer... progress) {}
	protected void onPostExecute(String r) {}
}