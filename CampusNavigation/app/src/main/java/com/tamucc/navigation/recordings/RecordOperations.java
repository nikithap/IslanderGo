package com.tamucc.navigation.recordings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.os.Environment;

import com.tamucc.navigation.database.DB_Route;
import com.tamucc.navigation.geometry.GoogleLatLngDistance;
import com.tamucc.navigation.routefilter.VelocityFinder;
import com.tamucc.navigation.routefilter.Location_Tamucc;
import com.google.android.gms.maps.model.LatLng;

public class RecordOperations {

    private File path = null;
    private File fileName = null;
    private File fileName_p = null;
    private String filePath = null;
    private String filePath_p = null;
    private FileWriter fileWritter;
    private FileWriter fileWritter_p;
    private BufferedWriter bufferWritter;
    private BufferedWriter bufferWritter_p;
    private BufferedReader bufferReader;
    private boolean canW, canR;

    public RecordOperations() {

    }


    public ArrayList<LatLng> readPointsFile(String fn) {
        ArrayList<LatLng> listOfPoints = new ArrayList<LatLng>();
        checkDownloadFolderExist();
        try {
            BufferedReader br = new BufferedReader(new FileReader(path + "/"
                    + fn + ".txt"));
            double lat, lng;
            String line = br.readLine();
            while (line != null) {
                String[] tmp = line.split(";");
                for (String tmpS : tmp) {
                    String[] tmpTwo = tmpS.split(",");
                    lat = Double.parseDouble(tmpTwo[0]);
                    lng = Double.parseDouble(tmpTwo[1]);
                    listOfPoints.add(new LatLng(lat, lng));
                }
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listOfPoints;
    }

    /**
     * file init
     *
     * @param extension
     */
    public void fileInitialization(String extension) {
        checkDownloadFolderExist();
        checkState();
        if (RWTrue()) {
            try {
                fileName = new File(path + "/" + "MyRoute1." + extension);
                for (int i = 2; i < 100; i++) {
                    if (fileName.exists()) {
                        fileName = new File(path + "/" + "MyRoute" + i + "."
                                + extension);
                    } else {
                        fileName.createNewFile();
                        filePath = fileName.getPath();
                        break;
                    }
                }

                fileWritter = new FileWriter(fileName, true);
                bufferWritter = new BufferedWriter(fileWritter);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void file_p_Initialization(String extension, File fn, String beta,
                                       boolean append) {
        checkDownloadFolderExist();
        System.out.println("****get file name: " + fn.getName());

        try {
            String tmp = fn.getName().replaceAll("." + extension,
                    "_" + beta + "." + extension);
            System.out.println("****tmp: " + tmp);
            fileName_p = new File(path + "/" + tmp);
            fileName_p.createNewFile();
            filePath_p = fileName_p.getPath();
            System.out.println("****Stored processed file_p path: "
                    + filePath_p);
            fileWritter_p = new FileWriter(fileName_p, append);// true:append
            // file
            bufferWritter_p = new BufferedWriter(fileWritter_p);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * testing method
     *
     * @param originalFileName
     */
    public void TESTING(String originalFileName) {
        checkDownloadFolderExist();
        filePath = path + "/" + originalFileName + ".txt";
        fileName = new File(path + "/" + originalFileName + ".txt");

        //processRecord_delete_consecutive();
        processRecord_kalman_filter("a", true);

        for (int i = 0; i < 29; i++)
            processRecord_kalman_filter("a", false);


    }



    public void processRecord_kalman_filter(String beta, boolean firstTime) {// Kalman

        try {
            String tmpF;
            if (firstTime) {
                tmpF = filePath;
            } else {
                tmpF = filePath_p;
            }
            bufferReader = new BufferedReader(new FileReader(tmpF));
            String line = bufferReader.readLine();
            file_p_Initialization("txt", fileName, beta, false);// save as

            while (line != null) {
                String[] tmp = line.split(";");

                VelocityFinder kf = new VelocityFinder();
                Location_Tamucc current;

                for (int i = 0; i < tmp.length; i++) {
                    current = new Location_Tamucc(tmp[i]);
                    kf.calculate(current.getX(), current.getY(), 1,
                            current.getTS());
                    appendDataIntoFile_p(kf.toString());
                }
                line = bufferReader.readLine();
            }
            System.out.println("prcessed Kalman Filter!!..");
            bufferReader.close();
            bufferWritter_p.close();
            fileWritter_p.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DB_Route calculate_distance_time_andGet_StartEndLatLng() {
        DB_Route returnRouteObj = null;
        try {
            bufferReader = new BufferedReader(new FileReader(filePath_p));
            String line = bufferReader.readLine();

            while (line != null) {
                String[] tmp = line.split(";");

                if (tmp.length < 2) {
                    return null;
                }

                String[] tmpFirst = tmp[0].split(",");
                String[] tmpLast = tmp[tmp.length - 1].split(",");
                double first_lat = Double.parseDouble(tmpFirst[0]);
                double first_lng = Double.parseDouble(tmpFirst[1]);
                double last_lat = Double.parseDouble(tmpLast[0]);
                double last_lng = Double.parseDouble(tmpLast[1]);

                long first_time = Long.parseLong(tmpFirst[2]);
                long last_time = Long.parseLong(tmpLast[2]);
                long delta = last_time - first_time;

                // time,unit: second
                long takeTime = TimeUnit.MILLISECONDS.toSeconds(delta);

                // calculate distance
                Location_Tamucc current1;
                Location_Tamucc current2;
                double distance = 0;

                // distance between two LatLng points
                GoogleLatLngDistance glld = new GoogleLatLngDistance();

                for (int i = 0; i < tmp.length - 1; i++) {
                    current1 = new Location_Tamucc(tmp[i]);
                    current2 = new Location_Tamucc(tmp[i + 1]);

                    distance += glld.GetDistance(current1.getX(),
                            current1.getY(), current2.getX(), current2.getY());
                }
                // else not append and move onto next line
                line = bufferReader.readLine();

                returnRouteObj = new DB_Route(first_lat, first_lng, last_lat,
                        last_lng, distance, takeTime);

            }
            System.out.println("Calculate a bunch of stuffs!");
            bufferReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnRouteObj;
    }

    // not in use
    public void processRecord_delete_outliers(String beta, boolean firstTime) {
        try {
            String tmpF;
            if (firstTime) {
                tmpF = filePath;
            } else {
                tmpF = filePath_p;
            }

            bufferReader = new BufferedReader(new FileReader(tmpF));
            String line = bufferReader.readLine();
            file_p_Initialization("txt", fileName, beta, false);// save as
            while (line != null) {
                String[] tmp = line.split(";");
                if (tmp.length >= 3) {

                    Location_Tamucc first = new Location_Tamucc(tmp[0]);
                    Location_Tamucc second = new Location_Tamucc(tmp[1]);
                    Location_Tamucc Li;
                    Location_Tamucc tmpP;
                    for (int i = 2; i < tmp.length; i++) {
                        Li = new Location_Tamucc(tmp[i]);
                        if (first.checkNextPointInScope(second, Li)) {
                        } else {
                            tmpP = first.getMidPoint();
                            if (tmpP != null)
                                second = tmpP;
                            tmp[i - 1] = second.toString();
                        }
                        first = second;
                        second = Li;
                    }
                    // now can store tmp into file again
                    for (String f : tmp) {
                        appendDataIntoFile_p(f + ";");
                    }
                    line = bufferReader.readLine();
                }
            }
            System.out.println("prcessed delete outliers..");
            bufferReader.close();
            bufferWritter_p.close();
            fileWritter_p.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void appendDataIntoFile(String data) {
        try {
            bufferWritter.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void appendDataIntoFile_p(String data) {
        try {
            // just execute writing
            System.out.println("Appending data"+data);
            bufferWritter_p.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void closeBufferWriter() {
        try {
            // close bw
            bufferWritter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeFileWriter() {
        try {
            fileWritter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getFileName() {
        return fileName.getName();
    }

    public String getProcessedFileName() {
        return fileName_p.getName();
    }

    public boolean RWTrue() {
        return canW && canR;
    }

    private void checkState() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            canW = canR = true;
        } else if (state.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            canW = false;
            canR = true;
        } else {
            canW = false;
            canR = false;
        }
    }

    public void checkDownloadFolderExist() {
        path = Environment
                .getExternalStoragePublicDirectory("CampusMap/Routes");
        path.mkdirs();
    }
}
