package com.tamucc.navigation;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.tamucc.navigation.database.DB_Building;
import com.tamucc.navigation.database.DB_Operations;
import com.tamucc.navigation.model.Info;
import com.tamucc.navigation.model.EventsAndInfos;
import com.tamucc.navigation.service.RestClient;
import com.tamucc.navigation.service.TamuccNavigationAPI;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoActivity extends BaseActivity {

    private List<Info> infosList;
    private ListView LV;

    Map<String, DB_Building> buildingMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        setTitle("Info list");
        readRoutesInfoFromDatabase();
        populateLV();
        getEventData();

    }

    public void getEventData() {

        TamuccNavigationAPI apiService =
                RestClient.getClient().create(TamuccNavigationAPI.class);

        showLoader();

        Call<EventsAndInfos> call = apiService.eventsandinfos();
        call.enqueue(new Callback<EventsAndInfos>() {
            @Override
            public void onResponse(Call<EventsAndInfos> call, Response<EventsAndInfos> response) {
                cancelLoader();

                infosList = (List<Info>) response.body().getInfo();
                System.out.println("EVENTS LIST" + infosList.size());
                ArrayAdapter<Info> adapter = new MyListAdapter();
                LV.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<EventsAndInfos> call, Throwable t) {
                t.printStackTrace();
                System.out.println("ERRORRR");
                cancelLoader();

            }
        });


    }


    private void populateLV() {
        LV = findViewById(R.id.routeLV);

        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> lv, View view,
                                    int position, long id) {
                //....click item event
                String fileName = LV.getItemAtPosition(position)
                        .toString();

                broadcastMsg(fileName);
                finish();
            }
        });
    }

    private void broadcastMsg(String bn) {
        sendMessageToHomeActivity("InfoActivity", bn);
    }


    private void sendMessageToHomeActivity(String activity, String bn) {
        Intent intent = new Intent("GetDirection");
        intent.putExtra("Activity", activity);
        intent.putExtra("FileName", bn);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private class MyListAdapter extends ArrayAdapter<Info> {
        public MyListAdapter() {
            super(InfoActivity.this, R.layout.activity_route_listviewitem,
                    infosList);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(
                        R.layout.info_item, parent, false);
            }

            // Find route to work with
            Info currentRoute = infosList.get(position);

            // Fill the route name
//            TextView nameText = itemView
//                    .findViewById(R.id.route_item_name);
//            nameText.setText(currentRoute.getRid() + ".to:");

            //route destination
            TextView destinationText = itemView
                    .findViewById(R.id.route_item_destination);
            destinationText.setText(currentRoute.getBuilding());

            TextView information = itemView.findViewById(R.id.information);
            information.setText(currentRoute.getInformation());


            ImageView imgRider = itemView.findViewById(R.id.imgRider);

            System.out.println("****" + currentRoute.getBuildingname());


            imgRider.setImageResource(buildingMap.get(currentRoute.getBuilding()).getBuildingIcon());
            return itemView;

        }

    }


    private void readRoutesInfoFromDatabase() {
        DB_Operations datasource = new DB_Operations(this);
        datasource.open();

        buildingMap = datasource.getBuilding();

        //setListAdapter(new ArrayAdapter<String>(RoutesActivity.this,
        //		android.R.layout.simple_list_item_1, datasource.getRouteInfo()));
        datasource.close();
    }


}
