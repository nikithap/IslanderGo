package com.tamucc.navigation;

import android.Manifest;
import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;

import com.tamucc.navigation.database.DB_Operations;
import com.tamucc.navigation.database.DatabaseEntry;
import com.tamucc.navigation.fonts.ScrimInsetsFrameLayout;
import com.tamucc.navigation.fonts.SlidingTabLayout;
import com.tamucc.navigation.util.UtilsDevice;
import com.tamucc.navigation.util.UtilsMiscellaneous;

import java.io.File;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class MainActivity extends AppCompatActivity {
    // private DB_Operations datasource;
    private TabHost mTabHost;
    private TabWidget mTabWidget;
    private LocalActivityManager lam;

    private Intent homeIntent;
    private Intent searchIntent;
    private Intent settingIntent;
    private Intent routeIntent;

    ViewPager pager;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"Map", "Places"};
    int Numboftabs = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    15);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    15);
        }


        databaseInitCheck();
        init_navigator();

        // set up the entire UI framework
        lam = new LocalActivityManager(MainActivity.this, false);
        lam.dispatchCreate(savedInstanceState);
        //  init_slider();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 15:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission granted.
                    //Continue with writing files...

                } else {
                    //Permission denied.
                }
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mTabHost.setCurrentTab(0);
        }
    };

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        lam.dispatchPause(isFinishing());
        super.onPause();
    }

    @Override
    protected void onResume() {
        lam.dispatchResume();
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);


        return true;
    }

    Toolbar toolbar;

    private void databaseInitCheck() {
        // check tables,if not exists, create
        DatabaseEntry dbe = new DatabaseEntry(this);
        dbe.createTables();

        File dbFile = this.getDatabasePath("CampusMap_Database.db");
        System.out.println("****" + dbFile.exists());
        System.out.println("****" + dbFile.getAbsolutePath());

        // check building_table has rows or not, if not exists, insert
        DB_Operations op = new DB_Operations(this);
        op.open();
        op.DB_init();
        op.uploadPreviousFailedRoute();
        op.getDBPath();
        //op.getCenterPointsFromBuildings();
        op.close();

    }

    private void init_navigator() {
        // Navigation Drawer
        DrawerLayout mDrawerLayout = findViewById(R.id.main_activity_DrawerLayout);
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.colorPrimary));
        ScrimInsetsFrameLayout mScrimInsetsFrameLayout = findViewById(R.id.main_activity_navigation_drawer_rootLayout);

        ActionBarDrawerToggle mActionBarDrawerToggle = new ActionBarDrawerToggle
                (

                        this,
                        mDrawerLayout,
                        toolbar,
                        R.string.navigation_drawer_opened,
                        R.string.navigation_drawer_closed
                ) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // Disables the burger/arrow animation by default
                super.onDrawerSlide(drawerView, 0);
            }
        };

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        mActionBarDrawerToggle.syncState();

        // Navigation Drawer layout width
        int possibleMinDrawerWidth = UtilsDevice.getScreenWidth(this) -
                UtilsMiscellaneous.getThemeAttributeDimensionSize(this, android.R.attr.actionBarSize);
        int maxDrawerWidth = getResources().getDimensionPixelSize(R.dimen.navigation_drawer_max_width);

        mScrimInsetsFrameLayout.getLayoutParams().width = Math.min(possibleMinDrawerWidth, maxDrawerWidth);
        // Set the first item as selected for the first time
        setTitle(R.string.toolbar_title_home);


    }


}
