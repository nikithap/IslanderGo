package com.tamucc.navigation.direction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.simonvt.messagebar.MessageBar;

import org.w3c.dom.Document;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.tamucc.navigation.MapsActivity;
import com.tamucc.navigation.R;
import com.tamucc.navigation.location.CurrentLocation;
import com.tamucc.navigation.mapdrawing.BuildingDrawing;
import com.tamucc.navigation.mapdrawing.PolylineDrawing;
import com.tamucc.navigation.routefilter.FilterRoutes;
import com.tamucc.navigation.routefilter.ReturnRoute;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.tamucc.navigation.util.Constants;

public class FetchRoutesAsync extends AsyncTask<String, Integer, String> {

    private Context ma;
    private LatLng fromPosition;
    private LatLng toPosition;
    private GoogleMap map;
    private FetchDirections cmd;

    private ArrayList<LatLng> googlePoints;
    private int googleDistance;
    private int googleTime;
    private BuildingDrawing bd;
    private ArrayList<Polyline> polylineAL;
    private boolean cameraToStart;
    private CurrentLocation ml;

    public FetchRoutesAsync(Context ma, GoogleMap map, LatLng fromPosition,
                            LatLng toPosition, BuildingDrawing bd,
                            boolean cameraToStart, CurrentLocation ml) {
        this.ma = ma;
        this.map = map;
        this.fromPosition = fromPosition;
        this.toPosition = toPosition;
        this.bd = bd;
        this.cameraToStart = cameraToStart;
        this.ml = ml;

        this.polylineAL = new ArrayList<Polyline>();

    }

    public ArrayList<Polyline> getPolyLineArrayList() {
        return polylineAL;
    }

    ProgressDialog progressBar;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = ProgressDialog.show(ma, "Wait",
                "Fetching routes for you", true);
    }

    @Override
    protected String doInBackground(String... arg0) {


        cmd = new FetchDirections();

        // send request to server
        cmd.fetchData(fromPosition, toPosition);

        // get google route
        FetchGoogleRoute md = new FetchGoogleRoute();
        Document doc = md.getDocument(fromPosition, toPosition,
                FetchGoogleRoute.MODE_WALKING);
        if (doc != null) {
            googlePoints = md.getDirection(doc,toPosition);
            googleDistance = md.getDistanceValue(doc);
            if(googleDistance==-1)
                return null;
            googleTime = md.getDurationValue(doc);
        }

        return null;
    }

    @Override
    protected void onPostExecute(String r) {
        super.onPostExecute(r);

        int row_to_optionMenu = 0;

        int status = cmd.getStatus();

        System.out.println("Status" + status);
        Bundle b = new Bundle();
        PolylineDrawing pdrawing = new PolylineDrawing();

        if (status == 1) {


            cmd.initRouteOBJ();

            List<ReturnRoute> rr = cmd.getRoutesArrayList();

            rr.add(new ReturnRoute(googleDistance, googleTime,
                    googlePoints, -1));

            FilterRoutes rop = new FilterRoutes(fromPosition, toPosition, bd);

            rr = rop.routeOptimize(rr, ml);

            if (rr.size() >= 3) {
                row_to_optionMenu = 3;
            } else {
                row_to_optionMenu = rr.size();

            }

            // sort
            Collections.sort(rr);

            b.putInt("onMsgClick", 1);
            b.putInt("NumberOfRoutes", row_to_optionMenu);

            int[] Colors = {Color.RED, Color.BLUE, Color.BLACK};
            int j = 0;
            for (ReturnRoute tmprr : rr) {
                if (j >= 3)
                    break;
                Polyline pl = pdrawing.drawLineOnGoogleMap(map,
                        tmprr.getPoints(), Colors[j], Constants.routeWidth);
                polylineAL.add(pl);
                b.putInt("distance_" + (j + 1), tmprr.getDistance());
                b.putInt("time_" + (j + 1), tmprr.getTakeTime());

                j++;
            }

        } else {
            row_to_optionMenu = 1;
            Polyline pl = pdrawing.drawLineOnGoogleMap(map, googlePoints,
                    Color.RED, Constants.routeWidth);
            polylineAL.add(pl);

            b.putInt("onMsgClick", 1);
            b.putInt("NumberOfRoutes", row_to_optionMenu);

            b.putInt("distance_1", googleDistance);
            b.putInt("time_1", googleTime);
        }

        if (cameraToStart)
            map.animateCamera(CameraUpdateFactory.newLatLng(fromPosition));
        else
            map.animateCamera(CameraUpdateFactory.newLatLng(toPosition));

        System.out.println("Got routes");
        progressBar.dismiss();

        Toast.makeText(ma, "request route success!", Toast.LENGTH_SHORT).show();

        if(ma instanceof MapsActivity) {
            ((MapsActivity)ma).createPopUp(b);
        }

    }

}