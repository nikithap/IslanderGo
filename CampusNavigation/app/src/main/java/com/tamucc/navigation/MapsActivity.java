package com.tamucc.navigation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//import net.simonvt.messagebar.MessageBar;

import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.CameraPosition;
import com.tamucc.navigation.database.DB_Helper;
import com.tamucc.navigation.database.DB_Operations;
import com.tamucc.navigation.database.DatabaseEntry;
import com.tamucc.navigation.direction.GoogleRouteAsync;
import com.tamucc.navigation.direction.Route;
import com.tamucc.navigation.direction.FetchRoutesAsync;
import com.tamucc.navigation.fonts.EditText_Helvatica_Meidum;
import com.tamucc.navigation.model.Event;
import com.tamucc.navigation.model.EventsAndInfos;
import com.tamucc.navigation.model.Info;
import com.tamucc.navigation.model.MapMode;
import com.tamucc.navigation.recordings.RecordOperations;
import com.tamucc.navigation.fonts.ScrimInsetsFrameLayout;
import com.tamucc.navigation.geometry.GoogleLatLngDistance;
import com.tamucc.navigation.helper.DistanceConvert;
import com.tamucc.navigation.helper.TimeConvert;
import com.tamucc.navigation.location.CurrentLocation;
import com.tamucc.navigation.location.CurrentLocation.LocationResult;
import com.tamucc.navigation.location.CurrentLocationAsync;
import com.tamucc.navigation.mapdrawing.BuildingDrawing;
import com.tamucc.navigation.mapdrawing.BuildingDrawing.Building;
import com.tamucc.navigation.mapdrawing.MarkerAndPolyLine;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.tamucc.navigation.service.RestClient;
import com.tamucc.navigation.service.Session;
import com.tamucc.navigation.service.TamuccNavigationAPI;
import com.tamucc.navigation.util.UtilsDevice;
import com.tamucc.navigation.util.UtilsMiscellaneous;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback, OnMapClickListener,
        OnMapLongClickListener, OnInfoWindowClickListener {

    private GoogleMap map;
    private ArrayList<LatLng> LongClickArrayPoints;
    private CurrentLocation ml;
    private CurrentLocationAsync locationTask;
    private BuildingDrawing bd;
    private Marker currentMarker;
    private Marker LongClickMarkerA;
    private Marker LongClickMarkerB;
    private Polyline LongClickPolyLine;
    private MarkerAndPolyLine marker_polyline;
    private int LongClickCount;
    private Location myLastLocation;
    //   private MessageBar mMessageBar;
    private LocationManager lm;
    private String destination;
    private DB_Helper dbh;
    private GoogleLatLngDistance glld;
    private TextView txtSearch;
    private int mode = MapMode.navigationMode;

    public EditText_Helvatica_Meidum drop;
    public EditText_Helvatica_Meidum pickUp;

    private Polyline lineFromRouteActivity;
    private FetchRoutesAsync campusRouteTask;
    private GoogleRouteAsync googleDirectionTask;
    private ArrayList<Polyline> RequestPolyLines;

    private boolean canRemoveThePolyline = true;
    Bundle savedInstanceState;

    public List<Info> infosList;
    public List<Event> eventsList;


    LinearLayout linearSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map);

        drop = findViewById(R.id.drop);
        pickUp = findViewById(R.id.pickUp);
        linearSearch = findViewById(R.id.linearSearch);

        txtSearch = findViewById(R.id.txtSearch);
        linearSearch.bringToFront();
        txtSearch.bringToFront();
        linearSearch.setVisibility(View.INVISIBLE);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    15);
        }

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    15);
            //System.out.println("INIT APP");
            //initapp();
        }

        getEventData();
    }

    public void initapp() {

        databaseInitCheck();
        init_navigator();


        // set up the entire UI framework
        LocalActivityManager lam = new LocalActivityManager(MapsActivity.this, false);
        lam.dispatchCreate(savedInstanceState);

        Bundle appBundle = savedInstanceState;
        dbh = new DB_Helper();
        glld = new GoogleLatLngDistance();

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                .getMapAsync(this);


        View locationButton = ((View) ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));

        // and next place it, for exemple, on bottom right (as Google Maps app)
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 80);
    }


    public void updateStartLocation() {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        System.out.println("MAP IS READY");

        map = googleMap;
        setUpBroadCastManager();
        mapInitialization();
        setUpListeners();
        messageBarInit();
        LongClickArrayPoints = new ArrayList<LatLng>();

        // initialize all the builing-drawing on the map
        bd = new BuildingDrawing(map);

        // camera to my current location
        findMyLocation();

        // options for drawing markers and polylines
        marker_polyline = new MarkerAndPolyLine(map);


    }

    private void messageBarInit() {
        //mMessageBar = new MessageBar(this);

    }

    private void GPS_Network_Initialization() {
        // abstract class, define its abstract method
        LocationResult locationResult = new LocationResult() {
            @Override
            public void gotLocation(Location location) {// callback
                if (location != null) {
                    myLastLocation = location;
                }
            }
        };
        ml = new CurrentLocation(this, map, bd);

        ml.setupLocation(this, locationResult);

    }

    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 12;

    private void findMyLocation() {// test, alternative

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSION_ACCESS_COARSE_LOCATION);
        }

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSION_ACCESS_COARSE_LOCATION);
            initLocation();
        }


    }


    public Location getLocation() {
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        String provider = lm.getBestProvider(criteria, true);
        Location myLocation = lm.getLastKnownLocation(provider);

        return myLocation;
    }

    public void initLocation() {
        Location myLocation = getLocation();
        if (myLocation != null) {
            setUpMyLocationCamera(myLocation, 17);
            myLastLocation = myLocation;
        }
        GPS_Network_Initialization();
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case MY_PERMISSION_ACCESS_COARSE_LOCATION:


                initLocation();
                break;

            case 15:
                System.out.println("INIT APP");


                initapp();
                break;
        }

    }

    public void submit(View view) {
        if (mode == MapMode.recordingMode) {
            if (ml != null) {
                linearSearch.setVisibility(View.INVISIBLE);

                String returnFileName = ml
                        .disableLocationUpdate(locationTask);
                System.out.println("DRAW ROUTES" + returnFileName);
                drawRoute(returnFileName, map, Color.RED);

            }
        } else {
            MaterialDialog dialog = new MaterialDialog.Builder(this)
                    .title("Select Route")
                    .items(routesList)
                    .itemsCallback(new MaterialDialog.ListCallback() {

                        @Override
                        public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {

                            Bundle b = new Bundle();
                            b.putInt("onMsgClick", 3);
                            startRoute(position + 1, "Picked Route" + position + 1, b);

                        }
                    }).show();
        }
    }

    private Location getLastKnownLocation() {
        lm = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = lm.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSION_ACCESS_COARSE_LOCATION);
            }

            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSION_ACCESS_FINE_LOCATION);
            }

            Location l = lm.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }


    /**
     * Two camera setup
     */
    private void setUpMyLocationCamera(Location l, int zoomto) {
        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(
                l.getLatitude(), l.getLongitude())));
        map.animateCamera(CameraUpdateFactory.zoomTo(zoomto));
    }

    private void setUpListeners() {
        map.setOnMapClickListener(this);
        map.setOnMapLongClickListener(this);
        map.setOnInfoWindowClickListener(this);// click snippet
    }

    private void mapInitialization() {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //	mapFragment.getMapAsync(this);
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            map.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onMapClick(LatLng point) {

        if (LongClickCount == 2 || LongClickArrayPoints.size() == 1) {
            clearMarkersAndLine();
        }


        // if the clicked point is in polygon
        if (bd.pointIsInPolygon(point)) {

            System.out.println("POINT IS IN POLYGON");
            Building tmpBuilding = bd.getCurrentTouchedBuilding();
            if (tmpBuilding != null) {
                addCampusBuildingMaker(point, tmpBuilding.getBuildingName(),
                        tmpBuilding.getAddress());
            }
        } else {// add a simple marker
            System.out.println("POINT IS OUT POLYGON");

            clearRouteActLine_DirectionLine();

            addSimpleMaker(point);
        }

        // Clears the previously MapLongClick position
        LongClickArrayPoints.clear();
        LongClickArrayPoints.add(point);
        LongClickCount++;
    }

    private void clearRouteActLine_DirectionLine() {

        // remove three big!
        if (lineFromRouteActivity != null) {
            lineFromRouteActivity.remove();
        }

        if (campusRouteTask != null) {
            RequestPolyLines = campusRouteTask.getPolyLineArrayList();
            if (RequestPolyLines.size() != 0) {
                // clear all of the previous lines
                for (Polyline tmpP : RequestPolyLines) {
                    tmpP.remove();
                }
            }
        }

        if (googleDirectionTask != null) {
            Polyline lastGoogleLine = googleDirectionTask.getDrawnLine();
            if (lastGoogleLine != null) {
                lastGoogleLine.remove();
            }
        }
    }

    private void addSimpleMaker(LatLng point) {
        MarkerOptions mo = marker_polyline.setupMarkerOptions(point,
                point.latitude + " , " + point.longitude,
                BitmapDescriptorFactory.HUE_RED, null, false);
        currentMarker = map.addMarker(mo);
        currentMarker.showInfoWindow();

    }

    private void addCampusBuildingMaker(LatLng point, String bn, String addr) {

        MarkerOptions mo = marker_polyline.setupMarkerOptions(point, bn,
                BitmapDescriptorFactory.HUE_RED, addr, true);
        currentMarker = map.addMarker(mo);
        currentMarker.showInfoWindow();
    }


    private void setUpBroadCastManager() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
                BuildingNameReceiver, new IntentFilter("GetDirection"));
    }

    private BroadcastReceiver BuildingNameReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            clearRouteActLine_DirectionLine();

            String activity = intent.getStringExtra("Activity");

            if (activity.equals("SearchActivity")) {
                System.out.println("SEARCH ACTIVITY");
                destination = intent.getStringExtra("BuildingName");
                // search the building lat & lng by bn

                drop.setText(destination);

                LatLng to = dbh.getCenterPointOfABuildingFromDB(destination);

                // start an ansync task
                if (myLastLocation != null) {
                    Location fromL = myLastLocation;
                    LatLng from = new LatLng(fromL.getLatitude(),
                            fromL.getLongitude());
                    // CallDirection(from, to);
                    fetchDirection(from, to, false);

                }
                txtSearch.setText("Start");

            } else if (activity.equals("RouteActivity")) {

                String filename = intent.getStringExtra("FileName");
                System.out.println("toString: ----   " + filename);
                // also draw the route as well

                DrawRouteFromRouteACT(filename, map, Color.RED);
            } else if (activity.equals("RecordActivity")) {

                txtSearch.setText("Stop Recording");
                mode = MapMode.recordingMode;
                linearSearch.setVisibility(View.VISIBLE);
                destination = intent.getStringExtra("BuildingName");
                startRecording();

            }

        }

    };


    @Override
    public void onMapLongClick(LatLng point) {
        MarkerOptions mo = marker_polyline.setupMarkerOptions(point, null,
                BitmapDescriptorFactory.HUE_RED, null, false);

        // set polyline in the map
        LongClickArrayPoints.add(point);

        if (LongClickCount == 2) {
            clearMarkersAndLine();
        }

        if (LongClickArrayPoints.size() == 1) {
            LongClickMarkerA = map.addMarker(mo);
        } else { // size is 2
            LongClickMarkerB = map.addMarker(mo);
        }

        if (LongClickArrayPoints.size() == 2) {// draw line

            PolylineOptions plo = marker_polyline.setupPolyLineOptions(
                    Color.RED, 5, LongClickArrayPoints);
            LongClickPolyLine = map.addPolyline(plo);

            // calculate distance
            double distance = glld.GetDistance(
                    LongClickArrayPoints.get(0).latitude,
                    LongClickArrayPoints.get(0).longitude,
                    LongClickArrayPoints.get(1).latitude,
                    LongClickArrayPoints.get(1).longitude);


            Toast.makeText(this, "Distance: " + distance,
                    Toast.LENGTH_SHORT).show();

            // start google direction async task
            CallDirection(LongClickArrayPoints.get(0),
                    LongClickArrayPoints.get(1));
            LongClickArrayPoints.clear();

        }
        LongClickCount++;
    }

    private void clearMarkersAndLine() {
        // remove the last line
        if (LongClickPolyLine != null) {
            LongClickPolyLine.remove();
        }
        // remove the last two markers
        if (LongClickMarkerA != null) {
            LongClickMarkerA.remove();
        }
        if (LongClickMarkerB != null) {
            LongClickMarkerB.remove();
        }
        // remove currentmarker
        if (currentMarker != null) {
            currentMarker.remove();
        }

        LongClickCount = 0;
        if (googleDirectionTask != null) {
            Polyline lastGoogleLine = googleDirectionTask.getDrawnLine();
            if (lastGoogleLine != null) {
                lastGoogleLine.remove();
            }
        }
    }

    private void fetchDirection(LatLng from, LatLng to,
                                boolean cameraToStart) {

        txtSearch.setText("Navigate");
        linearSearch.setVisibility(View.VISIBLE);

        System.out.println("******start------" + from);
        System.out.println("******end-------" + to);

        canRemoveThePolyline = true;

        clearRouteActLine_DirectionLine();

        canRemoveThePolyline = false;

        campusRouteTask = new FetchRoutesAsync(MapsActivity.this, map, from, to,
                bd, cameraToStart, ml);
        campusRouteTask.execute();

    }

    /**
     * Google direction
     */
    private void CallDirection(LatLng from, LatLng to) { // Async task
        googleDirectionTask = new GoogleRouteAsync(this, map, from, to);
        googleDirectionTask.execute();
    }

    private void drawRoute(String returnFileName, GoogleMap map, int color) {
        Route tmpR = new Route(new RecordOperations());
        tmpR.showTestRoute(returnFileName, map, color, false);
    }

    private void DrawRouteFromRouteACT(String returnFileName, GoogleMap map,
                                       int color) {

        // the filename is MyRouteX_a.txt, need to change to MyRouteX.txt
        String routeobjArr[] = returnFileName.split("=");
        String finalFN = routeobjArr[0].split("_")[0] + ".txt";

        Route tmpR = new Route(new RecordOperations());
        lineFromRouteActivity = tmpR.showTestRoute(finalFN, map, color, false);

        Bundle b = new Bundle();
        b.putInt("onMsgClick", 3);// after click the button, nothing happens

        TimeConvert tc = new TimeConvert(routeobjArr[2]);
//        mMessageBar.show(routeobjArr[1] + "M, " + tc, "Cancel",
//                R.drawable.ic_audiotrack_dark, b);

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        // destination string
        destination = marker.getTitle();

        drop.setText(destination);

        alertDialog.setTitle(destination);
        alertDialog.setMessage(marker.getSnippet());

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Info",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        // hide keyboard

                        // ----
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Get routes",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        LatLng from = new LatLng(myLastLocation.getLatitude(),
                                myLastLocation.getLongitude());

                        // destination center point

                        LatLng to = dbh
                                .getCenterPointOfABuildingFromDB(destination);

                        fetchDirection(from, to, true);

                    }

                });


        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        // ...

                    }
                });

        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.show();

    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onResume() {

        super.onResume();

    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                BuildingNameReceiver);

        // remove updates from Mylocation listeners
        if (ml != null) {
            ml.removeLMUpdate();
        }
        Toast.makeText(this, "Good Bye!", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }


    List<String> routesList = new ArrayList<>();

    public void createPopUp(Bundle appBundle) {
        int NumberOfRoutes = appBundle.getInt("NumberOfRoutes");
        routesList.clear();

        for (int i = 1; i <= NumberOfRoutes; i++) {
            String Name = null;
            if (i == 1) {
                Name = "Route 1";
            } else if (i == 2) {
                Name = "Route 2";
            } else if (i == 3) {
                Name = "Route 3";
            }
            TimeConvert tc = new TimeConvert(appBundle.getInt("time_" + i));
            DistanceConvert dc = new DistanceConvert(
                    appBundle.getInt("distance_" + i));
            //  mitem.setTitle();
            routesList.add(Name + ": " + dc + ", " + tc);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle b = new Bundle();
        b.putInt("onMsgClick", 3);
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.openDrawer(Gravity.LEFT);  // OPEN DRAWER
                } else {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);  // OPEN DRAWER

                }
                return true;


            case R.id.routeCancel:
                canRemoveThePolyline = true;
                break;

        }
        return true;
    }

    public void startRecording() {
        // Async task, begin the route

        drop.setText(destination);
        Toast.makeText(this, "Recording your route to" + destination, Toast.LENGTH_SHORT).show();

        locationTask = new CurrentLocationAsync(ml, destination);
        locationTask.execute();
    }

    private void startRoute(int routeNumber, String content, Bundle b) {

        canRemoveThePolyline = false;
        LatLng startNav = null;
        LatLng endNav = null;

        RequestPolyLines = campusRouteTask.getPolyLineArrayList();
        int i = 0;
        if (routeNumber == 1) {
            for (Polyline tmpP : RequestPolyLines) {
                if (i != 0)
                    tmpP.remove();
                else {
                    startNav = tmpP.getPoints().get(0);
                    endNav = tmpP.getPoints().get(tmpP.getPoints().size() - 1);
                }
                i++;
            }
        } else if (routeNumber == 2) {
            for (Polyline tmpP : RequestPolyLines) {
                if (i != 1)
                    tmpP.remove();
                else {
                    startNav = tmpP.getPoints().get(0);
                    endNav = tmpP.getPoints().get(tmpP.getPoints().size() - 1);

                }
                i++;
            }
        } else if (routeNumber == 3) {
            for (Polyline tmpP : RequestPolyLines) {
                if (i != 2)
                    tmpP.remove();
                else {
                    startNav = tmpP.getPoints().get(0);
                    endNav = tmpP.getPoints().get(tmpP.getPoints().size() - 1);

                }
                i++;
            }
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(startNav)

// Sets the center of the map to
                .zoom(17)                   // Sets the zoom
                .bearing((float) CurrentLocation.angleFromCoordinate(startNav.latitude, startNav.longitude, endNav.latitude, endNav.longitude))  // Sets the orientation of the camera to east
                .tilt(30)    // Sets the tilt of the camera to 30 degrees
                .build();    // Creates a CameraPosition from the builder
        map.animateCamera(CameraUpdateFactory.newCameraPosition(
                cameraPosition));
        // ....

    }

    Toolbar toolbar;

    private void databaseInitCheck() {
        DatabaseEntry dbe = new DatabaseEntry(this);
        dbe.createTables();

        File dbFile = this.getDatabasePath("CampusMap_Database.db");
        System.out.println("****" + dbFile.exists());
        System.out.println("****" + dbFile.getAbsolutePath());

        DB_Operations op = new DB_Operations(this);
        op.open();
        op.DB_init();
        op.uploadPreviousFailedRoute();
        op.getDBPath();
        //op.getCenterPointsFromBuildings();
        op.close();

        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
    }

    public void getEventData() {
        showLoader();

        TamuccNavigationAPI apiService =
                RestClient.getClient().create(TamuccNavigationAPI.class);

        Call<EventsAndInfos> call = apiService.eventsandinfos();
        call.enqueue(new Callback<EventsAndInfos>() {
            @Override
            public void onResponse(Call<EventsAndInfos> call, Response<EventsAndInfos> response) {
                cancelLoader();
                EventsAndInfos eventsAndInfos = response.body();
                infosList = eventsAndInfos.getInfo();
                eventsList = eventsAndInfos.getEvents();
            }

            @Override
            public void onFailure(Call<EventsAndInfos> call, Throwable t) {
                t.printStackTrace();
                cancelLoader();

            }
        });


    }

    private DrawerLayout mDrawerLayout;

    private void init_navigator() {
        // Navigation Drawer
        mDrawerLayout = findViewById(R.id.main_activity_DrawerLayout);
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.colorPrimary));
        ScrimInsetsFrameLayout mScrimInsetsFrameLayout = findViewById(R.id.main_activity_navigation_drawer_rootLayout);

        TextView navigation_drawer_account_information_display_name = mScrimInsetsFrameLayout.findViewById(R.id.navigation_drawer_account_information_display_name);
        navigation_drawer_account_information_display_name.setText(new Session(this).getUserName());

        ActionBarDrawerToggle mActionBarDrawerToggle = new ActionBarDrawerToggle
                (

                        this,
                        mDrawerLayout,
                        toolbar,
                        R.string.navigation_drawer_opened,
                        R.string.navigation_drawer_closed
                ) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0);
            }
        };


        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        mActionBarDrawerToggle.syncState();

        int possibleMinDrawerWidth = UtilsDevice.getScreenWidth(this) -
                UtilsMiscellaneous.getThemeAttributeDimensionSize(this, android.R.attr.actionBarSize);
        int maxDrawerWidth = getResources().getDimensionPixelSize(R.dimen.navigation_drawer_max_width);

        mScrimInsetsFrameLayout.getLayoutParams().width = Math.min(possibleMinDrawerWidth, maxDrawerWidth);
        setTitle(R.string.toolbar_title_home);


    }


    public void recordRoutes(View view) {

        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        Intent it = new Intent(this, RecordActivity.class);
        startActivity(it);
    }

    public void openSearch(View view) {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        Intent it = new Intent(this, SearchActivity.class);
        startActivity(it);
    }

    public void logout(View view) {

        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        Session session = new Session(this);
        session.logout();

    }

    public void openSettings(View view) {

        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        Intent it = new Intent(this, SettingActivity.class);
        startActivity(it);
    }


    public void openRoutes(View view) {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        Intent it = new Intent(this, RouteActivity.class);
        startActivity(it);
    }

    public void openEvents(View view) {


        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        Intent it = new Intent(this, EventsActivity.class);
        startActivity(it);
    }

    public void openInfo(View view) {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        Intent it = new Intent(this, InfoActivity.class);
        startActivity(it);
    }
}
