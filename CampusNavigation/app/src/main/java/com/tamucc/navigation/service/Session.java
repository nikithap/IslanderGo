package com.tamucc.navigation.service;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.tamucc.navigation.MainActivity;
import com.tamucc.navigation.MapsActivity;
import com.tamucc.navigation.WelcomeActivity;
import com.tamucc.navigation.model.User;
import com.tamucc.navigation.util.Constants;

public class Session {

    Context context;
    public SharedPreferences sharedpreferences;

    public Session(Context context) {

        this.context = context;
        sharedpreferences = context.getSharedPreferences(
                Constants.SHAREDPREF, Context.MODE_PRIVATE);
    }

    public boolean isUserloggedin() {
        if (sharedpreferences.contains(Constants.UID)) {

            return true;
        } else {
            return false;
        }
    }

    public String getUserName() {
        return sharedpreferences.getString(Constants.FULLNAME, "User Name");
    }


    public void login(User dto) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.USERNAME, dto.getUsername());
        editor.putInt(Constants.UID, dto.getUid());
        editor.putString(Constants.EMAIL, dto.getEmail());
        editor.putString(Constants.FULLNAME, dto.getFullname());
        editor.commit();


        Intent intent;

        intent = new Intent(context, MapsActivity.class);


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(intent);

    }

    public void logout() {

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();

        Intent intent = new Intent(context, WelcomeActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(intent);

    }
}
