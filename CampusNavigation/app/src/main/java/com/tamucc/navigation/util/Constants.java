package com.tamucc.navigation.util;


public class Constants {

    public static final String SHAREDPREF = "tamuccnavigation";
    public static final String UID = "uid";

    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String FULLNAME = "fullname";

    public static int routeWidth = 18;


}
