package com.tamucc.navigation.routefilter;

public class Location_Tamucc {
	private double x;
	private double y;
	private long ts;
	private Location_Tamucc middle;

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public long getTS() {
		return ts;
	}

	public Location_Tamucc(String unit) {
		String[] xyz = unit.split(",");
		x = Double.parseDouble(xyz[0]);
		y = Double.parseDouble(xyz[1]);
		ts = Long.parseLong(xyz[2]);
	}

	public Location_Tamucc(double x, double y, long ts) {
		this.x = x;
		this.y = y;
		this.ts = ts;
	}



	@Override
	public String toString() {
		return x + "," + y + "," + ts;
	}

	public double euclideanDistance(Location_Tamucc a, Location_Tamucc b) {
		double xDiff = a.x - b.x;
		double xSqr = Math.pow(xDiff, 2);

		double yDiff = a.y - b.y;
		double ySqr = Math.pow(yDiff, 2);

		double output = Math.sqrt(xSqr + ySqr);
		return output;
	}

	public Location_Tamucc getAverageMeanPoint(Location_Tamucc a, Location_Tamucc b,
											   Location_Tamucc second) {
		double newX = (a.x + b.x) / 2;
		double newY = (a.y + b.y) / 2;
		Long timeS = second.getTS();

		return new Location_Tamucc(newX, newY,timeS);
	}

	public boolean checkNextPointInScope(Location_Tamucc second, Location_Tamucc third) {
		Location_Tamucc mid = getAverageMeanPoint(this, third, second);
		this.middle = mid;
		double midToSecond = euclideanDistance(mid, second);
		double firstTomid = euclideanDistance(this, mid);

		if (midToSecond > firstTomid) {
			return false;
		} else
			return true;

	}

	public Location_Tamucc getMidPoint() {
		if (middle != null)
			return middle;
		else
			return null;
	}

}
