package com.tamucc.navigation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tamucc.navigation.service.Session;

public class WelcomeActivity extends BaseActivity {

    TextView signin;
    LinearLayout get;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        get = findViewById(R.id.get);
        signin = findViewById(R.id.signin);

        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(WelcomeActivity.this, RegistrationActivity.class);
                startActivity(it);

            }
        });

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(it);
            }
        });

    }

    protected void onResume() {
        super.onResume();
        Session session = new Session(WelcomeActivity.this);
        if (session.isUserloggedin()) {

            Intent intent;
            Context context = WelcomeActivity.this;
            intent = new Intent(context, MapsActivity.class);


            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_NEW_TASK);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);


        }
    }
}
