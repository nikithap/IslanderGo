package com.tamucc.navigation.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    public static final String BASE_URL = "http://ec2-52-14-40-139.us-east-2.compute.amazonaws.com:8080/api/";
    public static final String TP_BASE_URL = "http://ec2-52-14-40-139.us-east-2.compute.amazonaws.com/campusnavigation/";


    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
