package com.tamucc.navigation.recordings;

import android.os.Environment;

import com.tamucc.navigation.database.DB_Operations;
import com.tamucc.navigation.database.DB_Route;
import com.tamucc.navigation.service.RestClient;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SaveRecording {

    private int serverResponseCode = 0;
    private String ServerUri = null;
    private DB_Route dbr;

    private File uploadFilePath;
    private final String uploadFileName;
    private boolean uploadSucceed;


    public SaveRecording(DB_Route dbr) {

        this.dbr = dbr;

        uploadFileName = dbr.getFileName();

        uploadFilePath = checkDownloadFolderExist();

        ServerUri = RestClient.TP_BASE_URL + "/post.php";

    }

    public void upload() {
        uploadFile(uploadFilePath + "/" + uploadFileName);
    }

    public boolean isUploadSucceed() {
        return uploadSucceed;
    }

    public File checkDownloadFolderExist() {
        File tmp;
        tmp = Environment.getExternalStoragePublicDirectory("CampusMap/Routes");
        tmp.mkdirs();
        return tmp;
    }

    public int uploadFile(String sourceFileUri) {

        String fileName = sourceFileUri;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {
            System.out.println("Source File not exist :" + uploadFilePath + ""
                    + uploadFileName);
            return 0;
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(
                        sourceFile);
                URL url = new URL(ServerUri);
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type",
                        "multipart/form-data;boundary=" + boundary);

                String combinedString = dbr.getStarting_lat()
                        + "@" + dbr.getStarting_lng()
                        + "@" + dbr.getEnding_lat()
                        + "@" + dbr.getEnding_lng()
                        + "@" + dbr.getDistance()
                        + "@" + dbr.getTakeTime()
                        + "@" + dbr.getDestination()
                        + "@" + dbr.getFileName();

                System.out.println(combinedString);

                conn.setRequestProperty("upl", fileName);

                dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name='upl';filename='"
                        + combinedString + "'" + lineEnd);
                dos.writeBytes(lineEnd);

                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                System.out.println("HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {
                    System.out.println("Upload succeeded!");
                    uploadSucceed = true;
                } else {

                }
                fileInputStream.close();
                dos.flush();
                dos.close();
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

                // upload failure, might be network problem
                // Thus store the route in DB
                insertRouteIntoHistoryTable(uploadFileName);
                System.out.println("Upload failed, insert into db");

            }
            return serverResponseCode;
        } // End else block
    }

    private void insertRouteIntoHistoryTable(String fileName) {
        DB_Operations op = new DB_Operations();
        op.open();
        op.insertARoute(dbr, false);// need to add more attributes
        op.close();
    }
}
