package com.tamucc.navigation.recordings;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.tamucc.navigation.database.DB_Route;


public class SaveRecordingTask extends AsyncTask<Void, Integer, String> {

    private Context mContext;
    private DB_Route dbr;
    private SaveRecording fu;

    public SaveRecordingTask(DB_Route dbr, Context mContext) {
        this.dbr = dbr;
        this.mContext = mContext;
    }

    @Override
    protected String doInBackground(Void... arg0) {
        fu = new SaveRecording(dbr);
        fu.upload();
        return null;
    }

    @Override
    protected void onPostExecute(String r) {
        super.onPostExecute(r);
        if (fu.isUploadSucceed()) {
            Toast.makeText(mContext, "Uploaded to server",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(mContext, "No network. Will upload later.",
                    Toast.LENGTH_LONG).show();
        }
    }
}