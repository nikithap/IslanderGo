package com.tamucc.navigation.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseEntry extends SQLiteOpenHelper implements DB_Constants {

    private SQLiteDatabase db;
    public static final int DATABASE_VERSION = 6;
    public static final String DATABASE_NAME = CAMPUSMAP_DATABASE;

    public DatabaseEntry(Context context) {
        super(context, CAMPUSMAP_DATABASE, null, DATABASE_VERSION);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;
    }

    public void createTables() {
        db = this.getWritableDatabase();
        createBuildingTable(db);
        createRouteTable(db);
        createHistoryRouteTable(db);
        createInfoTable(db);
        this.close();
    }

    private void createBuildingTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + BUILDING_TABLE + " ("
                + BUILDING_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + BUILDING_NAME + " VARCHAR," + BUILDING_ADDRESS + " VARCHAR, "
                + LOCATION_LAT + " DOUBLE," + LOCATION_LNG + " DOUBLE, "
                + QUERY_TIME + " INTEGER default 0, "
                + BUILDING_ICON + " INTEGER, " + CREATE_TIME
                + " TimeStamp NOT NULL DEFAULT (datetime('now','localtime')), "
                + UPDATE_TIME
                + " TimeStamp NOT NULL DEFAULT (datetime('now','localtime'))"
                + ")");
    }

    private void createInfoTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + BUILDINGINFO_TABLE + " (" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                + BUILDING_NAME + " INTEGER,"
                + "INFO VARCHAR, "
                + UPDATE_TIME
                + " TimeStamp NOT NULL DEFAULT (datetime('now','localtime'))"
                + ")");
    }


    private void createRouteTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ROUTE_TABLE + " ("
                + ROUTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ROUTE_FILENAME + " VARCHAR, " + STARTING_LAT + " DOUBLE, "
                + STARTING_LNG + " DOUBLE, " + ENDING_LAT + " DOUBLE, "
                + ENDING_LNG + " DOUBLE, " + DISTANCE + " DOUBLE, " + TAKETIME
                + " INTEGER, " + DESTINATION
                + " VARCHAR, " + CREATE_TIME
                + " TimeStamp NOT NULL DEFAULT (datetime('now','localtime')), "
                + UPDATE_TIME
                + " TimeStamp NOT NULL DEFAULT (datetime('now','localtime'))"
                + ")");
    }

    private void createHistoryRouteTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ROUTE_HISTORY_TABLE + " ("
                + ROUTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ROUTE_FILENAME + " VARCHAR, " + STARTING_LAT + " DOUBLE, "
                + STARTING_LNG + " DOUBLE, " + ENDING_LAT + " DOUBLE, "
                + ENDING_LNG + " DOUBLE, " + DISTANCE + " DOUBLE, " + TAKETIME
                + " INTEGER, " + DESTINATION
                + " VARCHAR, " + CREATE_TIME
                + " TimeStamp NOT NULL DEFAULT (datetime('now','localtime')), "
                + UPDATE_TIME
                + " TimeStamp NOT NULL DEFAULT (datetime('now','localtime'))"
                + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + BUILDING_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ROUTE_TABLE);
        onCreate(db);
    }

}
