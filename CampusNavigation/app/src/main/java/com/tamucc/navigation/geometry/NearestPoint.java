package com.tamucc.navigation.geometry;

import java.util.ArrayList;
import java.util.Arrays;

import com.tamucc.navigation.mapdrawing.BuildingDrawing;
import com.google.android.gms.maps.model.LatLng;

public class NearestPoint {

    private LatLng origin;
    private ArrayList<LatLng> al;
    private ArrayList<LatLng> pointsForBuildingSet;

    public NearestPoint() {
    }

    public NearestPoint(LatLng origin, ArrayList<LatLng> al) {
        this.origin = origin;
        this.al = al;
        pointsForBuildingSet = new ArrayList<LatLng>();
    }

    public LatLng[] returnClosestFour() {
        LatLng closest_1 = getNearestPoint();
        LatLng closest_2 = getNearestPoint();
        LatLng closest_3 = getNearestPoint();
        LatLng closest_4 = getNearestPoint();
        return new LatLng[]{closest_1, closest_2, closest_3, closest_4};
    }

    public LatLng[] returnClosestTwoPoints(LatLng[] points) {

        pointsForBuildingSet.clear();
        for (LatLng tmpPoint : points)
            pointsForBuildingSet.add(tmpPoint);

        LatLng closest_1 = getNearestPointForTwo();
        LatLng closest_2 = getNearestPointForTwo();
        return new LatLng[]{closest_1, closest_2};
    }

    private LatLng getNearestPoint() {
        LatLng returnPoint = null;
        double min = Double.MAX_VALUE;
        int i = 0;
        int minIndex = Integer.MAX_VALUE;
        for (LatLng tmp : al) {
            double current = getDistance(tmp);

            if (current < min) {
                min = current;
                returnPoint = tmp;
                minIndex = i;
            }
            i++;
        }
        // delete the min element
        if (al.size() > minIndex)
            al.remove(minIndex);
        return returnPoint;
    }

    private LatLng getNearestPointForTwo() {
        LatLng returnPoint = null;
        double min = Double.MAX_VALUE;
        int i = 0;
        int minIndex = Integer.MAX_VALUE;
        for (LatLng tmp : pointsForBuildingSet) {
            double current = getDistance(tmp);

            if (current < min) {
                min = current;
                returnPoint = tmp;
                minIndex = i;
            }
            i++;
        }
        pointsForBuildingSet.remove(minIndex);
        return returnPoint;
    }


    public int getNearestPointForTwo(LatLng myLatLng, ArrayList<LatLng> rroute,
                                     BuildingDrawing bd) {

        double min = Double.MAX_VALUE;
        LatLng tmpLL;
        int minIndex = Integer.MAX_VALUE;
        for (int i = 0; i < rroute.size() / 2; i++) {

            tmpLL = rroute.get(i);
            double current = getDistance(myLatLng, tmpLL);

            if (!bd.pointIsInPolygon(tmpLL)) {
                if (current < min) {
                    min = current;
                    minIndex = i;
                }
            }

        }
        return minIndex;
    }

    public int getNearestPointForTwoReverse(LatLng centerLatLng, ArrayList<LatLng> rroute) {

        double min = Double.MAX_VALUE;
        LatLng tmpLL;
        int minIndex = Integer.MAX_VALUE;
        for (int i = rroute.size() - 1; i > rroute.size() / 2; i--) {

            tmpLL = rroute.get(i);
            double current = getDistance(centerLatLng, tmpLL);

            if (current < min) {
                min = current;
                minIndex = i;
            }


        }
        return minIndex;
    }

  
    private double getDistance(LatLng tmp) {

        double result = Math.pow(tmp.latitude - origin.latitude, 2)
                + Math.pow(tmp.longitude - origin.longitude, 2);
        result = Math.sqrt(result);
        return result;
    }

    private double getDistance(LatLng myLatLng, LatLng tmpLL) {
        double result = Math.pow(myLatLng.latitude - tmpLL.latitude, 2)
                + Math.pow(myLatLng.longitude - tmpLL.longitude, 2);
        //result = Math.sqrt(result);
        return result;
    }


}
