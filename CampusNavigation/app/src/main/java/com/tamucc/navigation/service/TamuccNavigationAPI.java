package com.tamucc.navigation.service;


import com.tamucc.navigation.model.EventsAndInfos;
import com.tamucc.navigation.model.User;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface TamuccNavigationAPI {

    @POST("login")
    Call<User> login(@Body User user);

    @POST("register")
    Call<User> register(@Body User user);

    @POST("eventsandinfos")
    Call<EventsAndInfos> eventsandinfos();

}
