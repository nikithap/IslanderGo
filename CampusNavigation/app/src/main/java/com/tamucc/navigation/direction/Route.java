package com.tamucc.navigation.direction;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import com.tamucc.navigation.recordings.RecordOperations;
import com.tamucc.navigation.mapdrawing.PolylineDrawing;
import com.tamucc.navigation.routefilter.Location_Tamucc;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

public class Route {
	private RecordOperations fo;
	private boolean recordIsStarted = false;
	private BlockingQueue<Location_Tamucc> buffer;


	public Route(RecordOperations fo) {
		buffer = new ArrayBlockingQueue<Location_Tamucc>(20);
		this.fo = fo;
	}

	public void startRecord(){
		recordIsStarted = true;
	}

	public Polyline showTestRoute(String justName, GoogleMap map, int color,boolean isOriginal) {
		String newS;
		if(isOriginal){
			newS = justName.replace(".txt", "");
		}else{
			newS = justName.replace(".txt", "_a");
		}
		ArrayList<LatLng> result = fo.readPointsFile(newS);
		
		LatLng first = result.get(0);
		
		PolylineDrawing pdrawing = new PolylineDrawing();
		
		Polyline pl = pdrawing.drawLineOnGoogleMap(map, result, color,15);
		
		map.animateCamera(CameraUpdateFactory.newLatLng(first));
 
		
		return pl;
	}

	public void bufferStore(Location_Tamucc location) {
		try {
			buffer.put(location);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	public void bufferTakeAndAddToFile() {
		Location_Tamucc tmp;
		try {
			tmp = buffer.take();
			String dataString = tmp.getX() + "," + tmp.getY()
					+ "," + tmp.getTS() + ";";
			fo.appendDataIntoFile(dataString);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	public void checkRemainingElementsInBQandCloseBuffer(
			RecordOperations deliverFile) {
		// put remaining ele in BQ
		while (!buffer.isEmpty()) {
			bufferTakeAndAddToFile();
		}
		// close bw
		deliverFile.closeBufferWriter();
		deliverFile.closeFileWriter();
	}
	
	public void toggleRecordState() {
		System.out.println("RECORD STATE"+recordIsStarted);
		recordIsStarted = !recordIsStarted;
	}

	// if a route recording is started
	public boolean recordHasStarted() {
		return recordIsStarted;
	}

}
