package com.tamucc.navigation.geometry;

import java.util.ArrayList;
import java.util.Map;

import android.content.Context;
import android.location.Location;

import com.tamucc.navigation.database.DB_Operations;
import com.tamucc.navigation.mapdrawing.BuildingDrawing;
import com.tamucc.navigation.mapdrawing.BuildingDrawing.Building;
import com.google.android.gms.maps.model.LatLng;

public class TargetBuilding {

    private Context mContext;
    private Location MyLastLocation;
    private BuildingDrawing bd;
    private LatLng enteredBuildingLatLng;

    public TargetBuilding(Context mContext, Location MyLastLocation,
                          BuildingDrawing bd) {
        this.mContext = mContext;
        this.MyLastLocation = MyLastLocation;
        this.bd = bd;
    }


    public Building getWhichBuildingEntered() {

        DB_Operations op = new DB_Operations(mContext);
        op.open();

        ArrayList<LatLng> al = op.getCenterPointsFromBuildings();

        LatLng origin = new LatLng(MyLastLocation.getLatitude(),
                MyLastLocation.getLongitude());

        NearestPoint nearestPoint = new NearestPoint(origin, al);

        LatLng[] returnFour = nearestPoint.returnClosestFour();

        int first_bid = op.getBidFromLatLng(returnFour[0]);
        int second_bid = op.getBidFromLatLng(returnFour[1]);
        int third_bid = op.getBidFromLatLng(returnFour[2]);
        int forth_bid = op.getBidFromLatLng(returnFour[3]);

        Map<Integer, Building> tmpBS = bd.getBuildingSet();

        Building firstb = tmpBS.get(first_bid);
        Building secondb = tmpBS.get(second_bid);
        Building thirdb = tmpBS.get(third_bid);
        Building forthb = tmpBS.get(forth_bid);

        TamuccDistance hd;

        LatLng[] F_returnTwo = nearestPoint.returnClosestTwoPoints(firstb.getPoints());

        hd = new TamuccDistance(origin, F_returnTwo[0], F_returnTwo[1]);
        double distance_1 = hd.getHaoDistance();

        LatLng[] S_returnTwo = nearestPoint.returnClosestTwoPoints(secondb.getPoints());

        hd = new TamuccDistance(origin, S_returnTwo[0], S_returnTwo[1]);
        double distance_2 = hd.getHaoDistance();

        LatLng[] T_returnTwo = nearestPoint.returnClosestTwoPoints(thirdb.getPoints());

        hd = new TamuccDistance(origin, T_returnTwo[0], T_returnTwo[1]);
        double distance_3 = hd.getHaoDistance();

        LatLng[] Four_returnTwo = nearestPoint.returnClosestTwoPoints(forthb.getPoints());

        hd = new TamuccDistance(origin, Four_returnTwo[0], Four_returnTwo[1]);
        double distance_4 = hd.getHaoDistance();


        double[] arr = {distance_1, distance_2, distance_3, distance_4};


        double minHaoDistance = Double.MAX_VALUE;
        int minIndex = Integer.MAX_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < minHaoDistance) {
                minHaoDistance = arr[i];
                minIndex = i;
            }
        }


        String bn1 = op.getBuildingNameFromLatLng(returnFour[0]);
        System.out.println(bn1 + " " + first_bid);
        String bn2 = op.getBuildingNameFromLatLng(returnFour[1]);
        System.out.println(bn2 + " " + second_bid);
        String bn3 = op.getBuildingNameFromLatLng(returnFour[2]);
        System.out.println(bn3 + " " + third_bid);
        String bn4 = op.getBuildingNameFromLatLng(returnFour[3]);
        System.out.println(bn4 + " " + forth_bid);

        op.close();

        if (minIndex == 0) {
            enteredBuildingLatLng = returnFour[0];
            return firstb;
        } else if (minIndex == 1) {
            enteredBuildingLatLng = returnFour[1];
            return secondb;
        } else if (minIndex == 2) {
            enteredBuildingLatLng = returnFour[2];
            return thirdb;
        } else if (minIndex == 3) {
            enteredBuildingLatLng = returnFour[3];
            return forthb;
        }
        return null;
    }

    public LatLng getEnteredBuildingLatLng() {
        return enteredBuildingLatLng;
    }
}
