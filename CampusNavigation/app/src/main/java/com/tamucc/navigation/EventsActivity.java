package com.tamucc.navigation;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.tamucc.navigation.database.DB_Building;
import com.tamucc.navigation.database.DB_Operations;
import com.tamucc.navigation.model.Event;
import com.tamucc.navigation.model.EventsAndInfos;
import com.tamucc.navigation.service.RestClient;
import com.tamucc.navigation.service.Session;
import com.tamucc.navigation.service.TamuccNavigationAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventsActivity extends BaseActivity {

    private List<Event> eventsList;
    private ListView LV;

    Map<String, DB_Building> buildingMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        setTitle("Events list");
        readRoutesInfoFromDatabase();
        populateLV();
        getEventData();

    }

    public void getEventData() {

        TamuccNavigationAPI apiService =
                RestClient.getClient().create(TamuccNavigationAPI.class);

        showLoader();

        Call<EventsAndInfos> call = apiService.eventsandinfos();
        call.enqueue(new Callback<EventsAndInfos>() {
            @Override
            public void onResponse(Call<EventsAndInfos> call, Response<EventsAndInfos> response) {
                cancelLoader();

                eventsList = (List<Event>) response.body().getEvents();
                System.out.println("EVENTS LIST"+eventsList.size());
                ArrayAdapter<Event> adapter = new MyListAdapter();
                LV.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<EventsAndInfos> call, Throwable t) {
                t.printStackTrace();
                System.out.println("ERRORRR");
                cancelLoader();

            }
        });


    }


    private void populateLV() {
        LV = findViewById(R.id.routeLV);

        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> lv, View view,
                                    int position, long id) {
                //....click item event
                String fileName = LV.getItemAtPosition(position)
                        .toString();

                broadcastMsg(fileName);
                finish();
            }
        });
    }

    private void broadcastMsg(String bn) {
        sendMessageToHomeActivity("EventsActivity", bn);
    }


    private void sendMessageToHomeActivity(String activity, String bn) {
        Intent intent = new Intent("GetDirection");
        intent.putExtra("Activity", activity);
        intent.putExtra("FileName", bn);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private class MyListAdapter extends ArrayAdapter<Event> {
        public MyListAdapter() {
            super(EventsActivity.this, R.layout.activity_route_listviewitem,
                    eventsList);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(
                        R.layout.event_listitem, parent, false);
            }

            // Find route to work with
            Event currentRoute = eventsList.get(position);

            // Fill the route name
//            TextView nameText = itemView
//                    .findViewById(R.id.route_item_name);
//            nameText.setText(currentRoute.getRid() + ".to:");

            //route destination
            TextView destinationText = itemView
                    .findViewById(R.id.route_item_destination);
            destinationText.setText(currentRoute.getBuildingname());

            TextView eventName = itemView.findViewById(R.id.eventName);
            eventName.setText(currentRoute.getEventname());


            TextView eventStartTime = itemView
                    .findViewById(R.id.eventStartTime);

            eventStartTime.setText(currentRoute.getStartdate().toString());

            //route distance and time
            TextView eventEndTime = itemView
                    .findViewById(R.id.eventEndTime);
            eventEndTime.setText(currentRoute.getEnddate().toString());

            ImageView imgRider = itemView.findViewById(R.id.imgRider);

            System.out.println("****"+currentRoute.getBuildingname());


            imgRider.setImageResource(buildingMap.get(currentRoute.getBuildingname()).getBuildingIcon());


            return itemView;

        }

    }


    private void readRoutesInfoFromDatabase() {
        DB_Operations datasource = new DB_Operations(this);
        datasource.open();

        buildingMap = datasource.getBuilding();

        //setListAdapter(new ArrayAdapter<String>(RoutesActivity.this,
        //		android.R.layout.simple_list_item_1, datasource.getRouteInfo()));
        datasource.close();
    }


}
