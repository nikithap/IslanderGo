package com.tamucc.navigation.direction;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;

import com.tamucc.navigation.mapdrawing.PolylineDrawing;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.tamucc.navigation.util.Constants;

import org.w3c.dom.Document;

import java.util.ArrayList;

public class GoogleRouteAsync extends AsyncTask<Void, Void, ArrayList<LatLng>> {

	private PolylineOptions rectline;
	private GoogleMap map;
	private LatLng fromPosition;
	private LatLng toPosition;
	private Polyline drawnLine;

	// constructor
	public GoogleRouteAsync(Context homeActivity, GoogleMap map,
                            LatLng fromPosition, LatLng toPosition) {
		Context mContext = homeActivity;
		this.map = map;
		this.fromPosition = fromPosition;
		this.toPosition = toPosition;
	}

	public Polyline getDrawnLine() {
		return drawnLine;
	}

	@Override
	protected ArrayList<LatLng> doInBackground(Void... params) {

		FetchGoogleRoute md = new FetchGoogleRoute();
		Document doc = md.getDocument(fromPosition, toPosition,
				FetchGoogleRoute.MODE_WALKING);
		if (doc != null) {
			ArrayList<LatLng> directionPoint = md.getDirection(doc,toPosition);
			return directionPoint;
		} else
			return null;
	}

	@Override
	protected void onPostExecute(ArrayList<LatLng> result) {
		super.onPostExecute(result);

		PolylineDrawing pdrawing = new PolylineDrawing();
		
		drawnLine = pdrawing.drawLineOnGoogleMap(map, result, Color.MAGENTA, Constants.routeWidth);
 

		map.animateCamera(CameraUpdateFactory.newLatLng(toPosition));

	}

}