package com.tamucc.navigation.routefilter;

public class VelocityFinder {

    private float metresPerSecond;
    private long unixTimeStamp;
    private double lat;
    private double lng;
    private float variance;

    public VelocityFinder() {
        this(3);
    }

    public VelocityFinder(float metresPerSecond) {
        this.metresPerSecond = metresPerSecond;
        variance = -1;
    }

    @Override
    public String toString() {
        return lat + "," + lng + "," + unixTimeStamp + ";";
    }


    public void calculate(double lat_measurement, double lng_measurement,
                          float accuracy, long unixTimeStamp) {
        float minimum = 1;
        if (accuracy < minimum)
            accuracy = minimum;
        if (variance < 0) {

            this.unixTimeStamp = unixTimeStamp;
            lat = lat_measurement;
            lng = lng_measurement;
            variance = accuracy * accuracy;
        } else {

            long timeinMillis = unixTimeStamp
                    - this.unixTimeStamp;
            if (timeinMillis > 0) {

                variance += timeinMillis * metresPerSecond
                        * metresPerSecond / 1000;
                this.unixTimeStamp = unixTimeStamp;

            }


            float K = variance / (variance + accuracy * accuracy);
            lat += K * (lat_measurement - lat);
            lng += K * (lng_measurement - lng);
            variance = (1 - K) * variance;
        }
    }
}
