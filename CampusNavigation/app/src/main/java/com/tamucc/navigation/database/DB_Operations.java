package com.tamucc.navigation.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.gms.maps.model.LatLng;
import com.tamucc.navigation.R;
import com.tamucc.navigation.recordings.SaveRecordingTask;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DB_Operations implements DB_Constants {

    private SQLiteDatabase database;
    private Context passed_context;

    public void getDBPath() {
        System.out.println("DB Path: " + database.getPath());
    }

    public DB_Operations() {
    }

    public DB_Operations(Context context) {
        passed_context = context;
    }

    public void open() throws SQLException {
        database = SQLiteDatabase.openOrCreateDatabase(
                "/data/user/0/com.tamucc.navigation/databases/CampusMap_Database.db", null, null);


    }

    public void close() {
        database.close();
    }

    private Cursor readData() {
        String[] FROM = {BUILDING_NAME, QUERY_TIME, BUILDING_ICON, UPDATE_TIME};
        String ORDER_BY = UPDATE_TIME + " DESC," + QUERY_TIME + " DESC,"
                + BUILDING_NAME + " ASC";
        Cursor cursor = database.rawQuery("select * from " + BUILDING_TABLE, null);
        return cursor;
    }

    private Cursor queryOneColumn_readDataFromABuildingName(String bn,
                                                            String columnName) {
        String[] FROM = {columnName};
        Cursor cursor = database.query(BUILDING_TABLE, FROM, BUILDING_NAME
                + "='" + bn + "'", null, null, null, null);
        return cursor;
    }

    private Cursor queryOneColumn_readDataFromAColumnName(String columnName) {

        // int queryValue = 0;
        String[] FROM = {columnName};
        Cursor cursor = database.query(BUILDING_TABLE, FROM, QUERY_TIME, null,
                null, null, null, null);

        return cursor;
    }

    private Cursor queryOneColumn_readDataFromALatLng(LatLng point,
                                                      String columnName) {
        String[] FROM = {columnName};
        Cursor cursor = database.query(BUILDING_TABLE, FROM, LOCATION_LAT
                + "='" + point.latitude + "' and " + LOCATION_LNG + "='"
                + point.longitude + "'", null, null, null, null);
        return cursor;
    }

    private Cursor readCenterPointFromBuildings() {
        String[] FROM = {LOCATION_LAT, LOCATION_LNG};
        Cursor cursor = database.query(BUILDING_TABLE, FROM, null, null, null,
                null, null);
        return cursor;
    }


    private Cursor readRouteData(boolean normal) {
        String table;
        if (normal) {
            table = ROUTE_TABLE;
        } else {
            table = ROUTE_HISTORY_TABLE;
        }
        String[] FROM = {ROUTE_ID, ROUTE_FILENAME, STARTING_LAT, STARTING_LNG,
                ENDING_LAT, ENDING_LNG, DISTANCE, TAKETIME, DESTINATION, CREATE_TIME};
        String ORDER_BY = CREATE_TIME + " DESC";
        Cursor cursor = database.query(table, FROM, null, null, null, null,
                ORDER_BY);
        return cursor;

    }

    private Cursor readBuildingData() {
        String[] FROM = {BUILDING_NAME, QUERY_TIME, BUILDING_ICON, UPDATE_TIME};
        String ORDER_BY = UPDATE_TIME + " DESC," + QUERY_TIME + " DESC,"
                + BUILDING_NAME + " ASC";
        Cursor cursor = database.rawQuery("select * from " + BUILDING_TABLE, null);
        return cursor;
    }


    public Map<String, DB_Building> getBuilding() {
        Cursor c = this.readData();

        Map<String, DB_Building> map = new HashMap<>();

        int iBN = c.getColumnIndex(BUILDING_NAME);
        int iQT = c.getColumnIndex(QUERY_TIME);
        int iBI = c.getColumnIndex(BUILDING_ICON);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {

            DB_Building dbb = new DB_Building(c.getString(iBN), c.getInt(iQT),
                    c.getInt(iBI));

            map.put(dbb.getBuildingName(), dbb);

        }
        c.close();

        return map;
    }

    public ArrayList<DB_Building> getBuildingOBJWithTimes() {
        Cursor c = this.readData();
        ArrayList<DB_Building> result = new ArrayList<DB_Building>();

        int iBN = c.getColumnIndex(BUILDING_NAME);
        int iQT = c.getColumnIndex(QUERY_TIME);
        int iBI = c.getColumnIndex(BUILDING_ICON);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {

            DB_Building dbb = new DB_Building(c.getString(iBN), c.getInt(iQT),
                    c.getInt(iBI));
            result.add(dbb);

        }
        return result;
    }

    public ArrayList<LatLng> getCenterPointsFromBuildings() {
        Cursor c = this.readCenterPointFromBuildings();
        ArrayList<LatLng> centerPoints = new ArrayList<LatLng>();
        int iLat = c.getColumnIndex(LOCATION_LAT);
        int iLng = c.getColumnIndex(LOCATION_LNG);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            centerPoints.add(new LatLng(c.getDouble(iLat), c.getDouble(iLng)));
            // System.out.println(c.getDouble(iLat)+" - "+c.getDouble(iLng));
        }
        return centerPoints;
    }


    public ArrayList<DB_Route> getRouteInfo(boolean normal) {
        Cursor c = this.readRouteData(normal);
        ArrayList<DB_Route> result = new ArrayList<DB_Route>();

        int iRid = c.getColumnIndex(ROUTE_ID);
        int iRfn = c.getColumnIndex(ROUTE_FILENAME);
        int iRslat = c.getColumnIndex(STARTING_LAT);
        int iRslng = c.getColumnIndex(STARTING_LNG);
        int iRelat = c.getColumnIndex(ENDING_LAT);
        int iRelng = c.getColumnIndex(ENDING_LNG);
        int iRd = c.getColumnIndex(DISTANCE);
        int iRtt = c.getColumnIndex(TAKETIME);
        int iRdes = c.getColumnIndex(DESTINATION);
        int iRCT = c.getColumnIndex(CREATE_TIME);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            int tt = c.getInt(iRtt);
            long taketime = Long.parseLong(String.valueOf(tt));

            result.add(new DB_Route(c.getInt(iRid), c.getString(iRfn),
                    c.getDouble(iRslat), c.getDouble(iRslng),
                    c.getDouble(iRelat), c.getDouble(iRelng),
                    c.getDouble(iRd), taketime, c.getString(iRdes),
                    c.getString(iRCT)));
        }
        return result;
    }

    public LatLng getLatLngFromDB(String bn) {
        String[] columns = new String[]{LOCATION_LAT, LOCATION_LNG};
        Cursor c = database.query(BUILDING_TABLE, columns, BUILDING_NAME + "='"
                + bn + "'", null, null, null, null);
        if (c.getCount() != 0) {
            int ilat = c.getColumnIndex(LOCATION_LAT);
            int ilng = c.getColumnIndex(LOCATION_LNG);
            c.moveToFirst();
            double lat = c.getDouble(ilat);
            double lng = c.getDouble(ilng);
            return new LatLng(lat, lng);
        } else
            return null;
    }

    // input a centerPoint LatLng, return its BN
    public String getBuildingNameFromLatLng(LatLng point) {
        Cursor c = this
                .queryOneColumn_readDataFromALatLng(point, BUILDING_NAME);
        if (c.getCount() != 0) {
            c.moveToFirst();
            int iBN = c.getColumnIndex(BUILDING_NAME);

            return c.getString(iBN);
        } else
            return null;
    }

    // input a centerPoint LatLng, return its Bid
    public int getBidFromLatLng(LatLng point) {
        if (point == null) {
            return -1;
        }
        Cursor c = this.queryOneColumn_readDataFromALatLng(point, BUILDING_ID);
        if (c.getCount() != 0) {
            c.moveToFirst();
            int iBid = c.getColumnIndex(BUILDING_ID);
            int bidno = c.getInt(iBid);
            c.close();
            return bidno;
        } else {
            c.close();
            return -1;
        }
    }


    public void insertARoute(DB_Route drb, boolean connected) {

        int take_time_converted = (int) drb.getTakeTime();

        ContentValues cv = new ContentValues();
        cv.put(ROUTE_FILENAME, drb.getFileName());
        cv.put(STARTING_LAT, drb.getStarting_lat());
        cv.put(STARTING_LNG, drb.getStarting_lng());
        cv.put(ENDING_LAT, drb.getEnding_lat());
        cv.put(ENDING_LNG, drb.getEnding_lng());
        cv.put(DISTANCE, drb.getDistance());
        cv.put(TAKETIME, take_time_converted);
        cv.put(DESTINATION, drb.getDestination());
        String table;
        if (connected) {
            table = ROUTE_TABLE;
        } else {
            table = ROUTE_HISTORY_TABLE;
        }
        database.insert(table, null, cv);
        System.out.println("inserted..");
    }


    public void updateQueryTime_setToNULL() {
        Cursor c = queryOneColumn_readDataFromAColumnName(QUERY_TIME);
        if (c.getCount() != 0) {
            c.moveToFirst();
            int qt = 0;

            ContentValues cv = new ContentValues();
            cv.put(QUERY_TIME, qt);

            database.update(BUILDING_TABLE, cv, null, null);
            System.out.println("updated! set to null..");
        }
    }

    public void updateQueryTimesForABuilding(String bn) {
        Cursor c = queryOneColumn_readDataFromABuildingName(bn, QUERY_TIME);
        if (c.getCount() != 0) {
            c.moveToFirst();
            int qt = c.getInt(0);
            qt++;
            ContentValues cv = new ContentValues();
            cv.put(QUERY_TIME, qt);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            cv.put(UPDATE_TIME, dateFormat.format(date));
            database.update(BUILDING_TABLE, cv,
                    BUILDING_NAME + "='" + bn + "'", null);
            System.out.println("updated..");
        }
    }


    public void deleteARoute(int id, boolean normal) {
        String table;
        if (normal) {
            table = ROUTE_TABLE;
        } else {
            table = ROUTE_HISTORY_TABLE;
        }
        database.delete(table, ROUTE_ID + "=" + id, null);
        System.out.println("deleted..");
    }

    public void deleteAllRoute() {
        String table;

        table = ROUTE_TABLE;

        database.delete(table, null, null);
        System.out.println("deleted all routes..");
    }

    public boolean BuildingTable_isEmpty() {
        String[] FROM = {BUILDING_ID};
        Cursor c = database.query(BUILDING_TABLE, FROM, null, null, null, null,
                null);
        if (c.getCount() == 0)
            return true;
        else
            return false;
    }

    public void DB_init() {
        if (BuildingTable_isEmpty() || getBuildingOBJWithTimes().size() == 12) {
            initialize_Building(BUILDING_NAME, BUILDING_ADDRESS, LOCATION_LAT,
                    LOCATION_LNG, BUILDING_ICON);
            initializeBuildingInfo();

        }

    }

    public void uploadPreviousFailedRoute() {
        ArrayList<DB_Route> al = this.getRouteInfo(false);
        if (al.size() > 0) {
            for (DB_Route tmp : al) {
                SaveRecordingTask uploadTask = new SaveRecordingTask(tmp, passed_context);
                uploadTask.execute();
                // and then delete the row in RouteHistory
                deleteARoute(tmp.getRid(), false);
            }
        }
    }

    private void initializeBuildingInfo() {
        database.execSQL("INSERT INTO BuildingInfo ( BuildingName,INFO" +
                ") VALUES ('Mary and Jeff Bell Library','This building is located at ....'" +
                ");");

        database.execSQL("INSERT INTO BuildingInfo ( BuildingName,INFO" +
                ") VALUES ('Mary and Jeff Bell Library','This building is located at ....'" +
                ");");

        database.execSQL("INSERT INTO BuildingInfo ( BuildingName,INFO" +
                ") VALUES ('Mary and Jeff Bell Library','This building is located at ....'" +
                ");");

        database.execSQL("INSERT INTO BuildingInfo ( BuildingName,INFO" +
                ") VALUES ('Mary and Jeff Bell Library','This building is located at ....'" +
                ");");

    }

    // insert all the building info into the Building table
    private void initialize_Building(String bn, String ba, String lat,
                                     String lng, String bi) {
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Mary and Jeff Bell Library','6300 Ocean Dr, Corpus Christi, TX 78412','27.713559','-97.324729',"
                + R.drawable.bi1 + ");");

        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Starfish Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.715443','-97.326871',"
                + R.drawable.bi2 + ");");


        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Center for the Sciences','6300 Ocean Dr, Corpus Christi, TX 78412','27.712691','-97.324935',"
                + R.drawable.bi3 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Engineering Building','6300 Ocean Dr, Corpus Christi, TX 78412','27.712718','-97.325640',"
                + R.drawable.bi4 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Glasscock Center','6300 Ocean Dr, Corpus Christi, TX 78412','27.713085','-97.325358',"
                + R.drawable.bi5 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Bayside Parking Garage','6300 Ocean Dr, Corpus Christi, TX 78412','27.713769','-97.326327',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Seahorse Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.714246','-97.327440',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Jellyfish Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.712491','-97.326803',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Center for the Arts','6300 Ocean Dr, Corpus Christi, TX 78412','27.713949','-97.323049',"
                + R.drawable.bi3 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('University Services Center','6300 Ocean Dr, Corpus Christi, TX 78412','27.714744','-97.323289',"
                + R.drawable.bi7 + ");");

        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Corpus Christi Hall','6300 Ocean Dr, Corpus Christi, TX 78412','27.714634','-97.324000',"
                + R.drawable.bi3 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Early Childhood Development Center','6300 Ocean Dr, Corpus Christi, TX 78412','27.715350','-97.325605',"
                + R.drawable.bi8 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Michael and Karen O Connor Building','6300 Ocean Dr, Corpus Christi, TX 78412','27.714329','-97.324848',"
                + R.drawable.bi9 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Student Services Center','6300 Ocean Dr, Corpus Christi, TX 78412','27.713710','-97.323874',"
                + R.drawable.bi10 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Bay Hall','6300 Ocean Dr, Corpus Christi, TX 78412','27.713520','-97.323491',"
                + R.drawable.bi3 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Faculty Center','6300 Ocean Dr, Corpus Christi, TX 78412','27.712948','-97.323980',"
                + R.drawable.bi11 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Carlos Truan Natural Resources Center','6300 Ocean Dr, Corpus Christi, TX 78412','27.715094','-97.328773',"
                + R.drawable.bi12 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Harte Research Institute','6300 Ocean Dr, Corpus Christi, TX 78412','27.716424','-97.328839',"
                + R.drawable.bi13 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('NRC Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.715550','-97.328522',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('HRI Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.715926','-97.328138',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Conrad Blucher Institute for Surveying/Science','6300 Ocean Dr, Corpus Christi, TX 78412','27.714531','-97.328780',"
                + R.drawable.bi14 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Science Lab','6300 Ocean Dr, Corpus Christi, TX 78412','27.714782','-97.329042',"
                + R.drawable.bi3 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Physical Plant','6300 Ocean Dr, Corpus Christi, TX 78412','27.713295','-97.327514',"
                + R.drawable.bi15 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Purchasing','6300 Ocean Dr, Corpus Christi, TX 78412','27.712714','-97.328530',"
                + R.drawable.bi3 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Angelfish Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.714538','-97.325628',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Classroom West','6300 Ocean Dr, Corpus Christi, TX 78412','27.714635','-97.326285',"
                + R.drawable.bi16 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('University Center','6300 Ocean Dr, Corpus Christi, TX 78412','27.711927','-97.325650',"
                + R.drawable.bi17 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Dugan Wellness Center','6300 Ocean Dr, Corpus Christi, TX 78412','27.711231','-97.324477',"
                + R.drawable.bi18 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Island Hall','6300 Ocean Dr, Corpus Christi, TX 78412','27.710803','-97.324215',"
                + R.drawable.bi19 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Turtle Cove Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.711015','-97.325860',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Field House','6300 Ocean Dr, Corpus Christi, TX 78412','27.711643','-97.323777',"
                + R.drawable.bi3 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Classroom East','6300 Ocean Dr, Corpus Christi, TX 78412','27.712841','-97.322502',"
                + R.drawable.bi20 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Dining Hall','6300 Ocean Dr, Corpus Christi, TX 78412','27.711337','-97.322599',"
                + R.drawable.bi21 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Center for Instruction','6300 Ocean Dr, Corpus Christi, TX 78412','27.712405','-97.324680',"
                + R.drawable.bi23 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Driftwood','6300 Ocean Dr, Corpus Christi, TX 78412','27.712456','-97.321694',"
                + R.drawable.bi24 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Sandpiper','6300 Ocean Dr, Corpus Christi, TX 78412','27.712339','-97.321369',"
                + R.drawable.bi22 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Hammerhead Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.713033','-97.319474',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Sanddollar Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.713641','-97.321272',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Seabreeze Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.713204','-97.322205',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Tarpon Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.712849','-97.321267',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Curlew Parking Lot','6300 Ocean Dr, Corpus Christi, TX 78412','27.712281','-97.323002',"
                + R.drawable.bi6 + ");");
        database.execSQL("INSERT INTO Building ("
                + bn
                + ","
                + ba
                + ","
                + lat
                + ","
                + lng
                + ","
                + bi
                + ") VALUES ('Performing Arts Center','6300 Ocean Dr, Corpus Christi, TX 78412','27.714330','-97.322551',"
                + R.drawable.bi26 + ");");


    }

}