package com.tamucc.navigation.database;

import android.provider.BaseColumns;

public interface DB_Constants extends BaseColumns {


    String ROUTE_TABLE = "Route";
    String ROUTE_ID = "Rid";
    String ROUTE_FILENAME = "RouteFileName";
    String STARTING_LAT = "Starting_lat";
    String STARTING_LNG = "Starting_lng";
    String ENDING_LAT = "Ending_lat";
    String ENDING_LNG = "Ending_lng";
    String DISTANCE = "Distance";
    String TAKETIME = "TakeTime";
    String DESTINATION = "Destination";

    String CREATE_TIME = "CreateTime";
    String UPDATE_TIME = "UpdateTime";

    String ROUTE_HISTORY_TABLE = "RouteHistory";

    String CAMPUSMAP_DATABASE = "CampusMap_Database.db";

    String BUILDING_TABLE = "Building";
    String BUILDING_ID = "Bid";
    String BUILDING_NAME = "BuildingName";
    String BUILDING_ADDRESS = "BuildingAddress";
    String LOCATION_LAT = "Location_lat";
    String LOCATION_LNG = "Location_lng";
    String QUERY_TIME = "QueryTime";
    String BUILDING_ICON = "BuildingIcon";

    String BUILDINGINFO_TABLE = "BuildingInfo";

}
