package com.tamucc.navigation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.tamucc.navigation.model.User;
import com.tamucc.navigation.service.RestClient;
import com.tamucc.navigation.service.Session;
import com.tamucc.navigation.service.TamuccNavigationAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    ImageView signinback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        signinback = findViewById(R.id.signinback);


        signinback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(LoginActivity.this, WelcomeActivity.class);
                startActivity(it);
            }
        });
    }

    public void doLogin(View view) {
        EditText username = findViewById(R.id.username);
        EditText password = findViewById(R.id.password);
        if (username.getText().toString().trim().equals(""))
            username.setError(getString(R.string.required_field));
        else if (password.getText().toString().trim().equals(""))
            password.setError(getString(R.string.required_field));
        else {
            User user = new User();
            user.setEmail(username.getText().toString().trim());
            user.setPassword(password.getText().toString().trim());
            login(user);
        }

    }

    public void login(User user) {
        showLoader();
        TamuccNavigationAPI apiService =
                RestClient.getClient().create(TamuccNavigationAPI.class);

        Call<User> call = apiService.login(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                cancelLoader();
                System.out.println("****" + response.body());
                System.out.println("****" + response.toString());
                if (response.code() != 200)
                    return;
                boolean error = response.body().getError();
                if (error) {
                    new MaterialDialog.Builder(LoginActivity.this)
                            .title("Failed")
                            .content("Invalid Login, Please try again")
                            .positiveText("Ok")
                            .show();

                } else {
                    Session session = new Session(LoginActivity.this);
                    session.login(response.body());

                    Intent it = new Intent(LoginActivity.this, MapsActivity.class);
                    startActivity(it);

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                cancelLoader();

            }
        });
    }
}
