package com.tamucc.navigation;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.tamucc.navigation.service.Session;

public class BaseActivity extends AppCompatActivity {

    ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

    }

    public ProgressDialog showLoader() {
        progressBar = ProgressDialog.show(this, "",
                "Please wait", true);
        return progressBar;
    }

    public void cancelLoader() {

        progressBar.dismiss();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        if (id == R.id.action_logout) {
            Session session = new Session(this);
            session.logout();
            return true;
        }

        return true;
    }
}
