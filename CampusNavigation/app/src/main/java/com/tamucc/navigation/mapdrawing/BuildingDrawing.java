package com.tamucc.navigation.mapdrawing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.tamucc.navigation.model.Event;
import com.tamucc.navigation.model.Info;

public class BuildingDrawing {

    private Map<Integer, Building> buildingSet;
    private GoogleMap map;
    private Building currentTouchedBuilding;
    public Map<String, Info> infosList;
    public Map<String, Event> eventsList;


    public BuildingDrawing(GoogleMap map) {

        buildingSet = new HashMap<Integer, Building>();
        this.map = map;
        drawAllTheCampusBuildings();
    }

    public Map<Integer, Building> getBuildingSet() {
        return buildingSet;
    }

    // ALGO: return true if point is in polygon
    public boolean pointIsInPolygon(LatLng touchPoint) {
        int i;
        int j;
        boolean result = false;
        System.out.println("BUILDING SET" + buildingSet.values().size());
        for (Building b : buildingSet.values()) {
            LatLng[] bp = b.getPoints();
            for (i = 0, j = bp.length - 1; i < bp.length; j = i++) {
                if ((bp[i].longitude > touchPoint.longitude) != (bp[j].longitude > touchPoint.longitude)
                        && (touchPoint.latitude < (bp[j].latitude - bp[i].latitude)
                        * (touchPoint.longitude - bp[i].longitude)
                        / (bp[j].longitude - bp[i].longitude)
                        + bp[i].latitude)) {
                    result = !result;
                }
            }
            if (result) {
                currentTouchedBuilding = b;
                return result;
            }
        }
        return result;
    }

    public Building getCurrentTouchedBuilding() {
        return currentTouchedBuilding;
    }

    private void drawAllTheCampusBuildings() {
        buildingSet.put(1, new Building("Mary and Jeff Bell Library", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713394, -97.325174),
                new LatLng(27.713949, -97.324906),
                new LatLng(27.713840, -97.324311),
                new LatLng(27.713171, -97.324561)));

        buildingSet.put(2, new Building("Starfish Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412", new LatLng(
                27.714650, -97.326744), new LatLng(27.715069, -97.327641),
                new LatLng(27.716632, -97.326842), new LatLng(27.716447,
                -97.326447)));

        buildingSet.put(3, new Building("Center for the Sciences", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713123, -97.324910),
                new LatLng(27.713008, -97.324662),
                new LatLng(27.712347, -97.324998),
                new LatLng(27.712451, -97.325250)
        ));
        buildingSet.put(4, new Building("Engineering Building", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712465, -97.325331),
                new LatLng(27.712659, -97.326044),
                new LatLng(27.713027, -97.325947),
                new LatLng(27.712720, -97.325157)
        ));
        buildingSet.put(5, new Building("Glasscock Center", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712806, -97.325104),
                new LatLng(27.713099, -97.324979),
                new LatLng(27.713297, -97.325470),
                new LatLng(27.713030, -97.325655)
        ));
        buildingSet.put(6, new Building("Bayside Parking Garage", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713138, -97.326176),
                new LatLng(27.714075, -97.325718),
                new LatLng(27.714456, -97.326543),
                new LatLng(27.713459, -97.326983)
        ));
        buildingSet.put(7, new Building("Seahorse Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713945, -97.328248),
                new LatLng(27.714863, -97.327804),
                new LatLng(27.714119, -97.327027),
                new LatLng(27.713591, -97.326997)
        ));
        buildingSet.put(8, new Building("Jellyfish Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713130, -97.326831),
                new LatLng(27.712141, -97.327427),
                new LatLng(27.711905, -97.326630),
                new LatLng(27.712823, -97.326333)
        ));
        buildingSet.put(9, new Building("Center for the Arts", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.714084, -97.323870),
                new LatLng(27.713574, -97.322662),
                new LatLng(27.713905, -97.322521),
                new LatLng(27.714270, -97.323786)
        ));
        buildingSet.put(10, new Building("University Services Center", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.714516, -97.323118),
                new LatLng(27.714840, -97.323280),
                new LatLng(27.714934, -97.323636),
                new LatLng(27.714793, -97.323664),
                new LatLng(27.714805, -97.323631),
                new LatLng(27.714492, -97.323265)
        ));
        buildingSet.put(11, new Building("Corpus Christi Hall", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.715145, -97.323640),
                new LatLng(27.714187, -97.324107),
                new LatLng(27.714273, -97.324359),
                new LatLng(27.714945, -97.324062)
        ));
        buildingSet.put(12, new Building("Early Childhood Development Center", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.715354, -97.324878),
                new LatLng(27.715169, -97.324751),
                new LatLng(27.715271, -97.325940),
                new LatLng(27.715716, -97.326025)
        ));
        buildingSet.put(13, new Building("Michael and Karen OConnor Building", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713999, -97.324796),
                new LatLng(27.714111, -97.325191),
                new LatLng(27.714566, -97.324983),
                new LatLng(27.714779, -97.324294),
                new LatLng(27.714462, -97.324418),
                new LatLng(27.714256, -97.324790)
        ));
        buildingSet.put(14, new Building("Student Services Center", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713906, -97.323992),
                new LatLng(27.713651, -97.324148),
                new LatLng(27.713513, -97.323788),
                new LatLng(27.713745, -97.323669)
        ));
        buildingSet.put(15, new Building("Bay Hall", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713766, -97.323531),
                new LatLng(27.713657, -97.323172),
                new LatLng(27.713010, -97.323455),
                new LatLng(27.713116, -97.323910)
        ));
        buildingSet.put(16, new Building("Faculty Center", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712672, -97.323773),
                new LatLng(27.712894, -97.323518),
                new LatLng(27.713218, -97.324340),
                new LatLng(27.712939, -97.324450)
        ));
        buildingSet.put(17, new Building("Carlos Truan Natural Resources Center", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.714986, -97.329176),
                new LatLng(27.714718, -97.328417),
                new LatLng(27.715140, -97.328270),
                new LatLng(27.715390, -97.328831)
        ));
        buildingSet.put(18, new Building("Harte Research Institute", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.716859, -97.328997),
                new LatLng(27.716527, -97.328317),
                new LatLng(27.716269, -97.328429),
                new LatLng(27.716560, -97.329242)
        ));
        buildingSet.put(19, new Building("NRC Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.715538, -97.328947),
                new LatLng(27.715834, -97.328933),
                new LatLng(27.715451, -97.328116),
                new LatLng(27.715188, -97.328176)
        ));
        buildingSet.put(20, new Building("HRI Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.715907, -97.328539),
                new LatLng(27.716214, -97.328351),
                new LatLng(27.715855, -97.327647),
                new LatLng(27.715605, -97.327848)
        ));
        buildingSet.put(21, new Building("Conrad Blucher Institute for Surveying/Science", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.714649, -97.328592),
                new LatLng(27.714364, -97.328695),
                new LatLng(27.714445, -97.329040),
                new LatLng(27.714691, -97.328897)
        ));
        buildingSet.put(22, new Building("Science Lab", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.714747, -97.328882),
                new LatLng(27.714749, -97.329188),
                new LatLng(27.714892, -97.329165),
                new LatLng(27.714819, -97.328867)
        ));
        buildingSet.put(23, new Building("Physical Plant", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713555, -97.328095),
                new LatLng(27.713172, -97.328473),
                new LatLng(27.712706, -97.327546),
                new LatLng(27.713272, -97.327244)
        ));
        buildingSet.put(24, new Building("Purchasing", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712556, -97.328493),
                new LatLng(27.712565, -97.328582),
                new LatLng(27.712854, -97.328673),
                new LatLng(27.712842, -97.328503)
        ));
        buildingSet.put(25, new Building("Angelfish Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.714677, -97.325429),
                new LatLng(27.714186, -97.325618),
                new LatLng(27.714356, -97.326006),
                new LatLng(27.714800, -97.325788)
        ));
        buildingSet.put(26, new Building("Classroom West", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.714616, -97.325947),
                new LatLng(27.714440, -97.326027),
                new LatLng(27.714701, -97.326506),
                new LatLng(27.714868, -97.326534)
        ));
        buildingSet.put(27, new Building("University Center", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.711938, -97.325007),
                new LatLng(27.711290, -97.325050),
                new LatLng(27.711884, -97.326583),
                new LatLng(27.712407, -97.326271)
        ));
        buildingSet.put(28, new Building("Dugan Wellness Center", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.711107, -97.324841),
                new LatLng(27.711772, -97.324592),
                new LatLng(27.711505, -97.324034),
                new LatLng(27.711193, -97.324109)
        ));
        buildingSet.put(29, new Building("Island Hall", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.711153, -97.323941),
                new LatLng(27.710591, -97.323436),
                new LatLng(27.710270, -97.323592),
                new LatLng(27.710556, -97.324158),
                new LatLng(27.710623, -97.324936),
                new LatLng(27.710965, -97.325101)
        ));
        buildingSet.put(30, new Building("Turtle Cove Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.710636, -97.325152),
                new LatLng(27.710617, -97.326096),
                new LatLng(27.711481, -97.326416),
                new LatLng(27.711196, -97.325434)
        ));
        buildingSet.put(31, new Building("Field House", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.711564, -97.324000),
                new LatLng(27.711843, -97.323860),
                new LatLng(27.711660, -97.323468),
                new LatLng(27.711198, -97.323892)
        ));
        buildingSet.put(32, new Building("Classroom East", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713010, -97.322772),
                new LatLng(27.712742, -97.322153),
                new LatLng(27.712642, -97.322244),
                new LatLng(27.712874, -97.322839)
        ));
        buildingSet.put(33, new Building("Dining Hall", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.710954, -97.322375),
                new LatLng(27.711372, -97.322184),
                new LatLng(27.711628, -97.322832),
                new LatLng(27.711309, -97.323034)
        ));
        buildingSet.put(34, new Building("Center for Instruction", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712507, -97.324702),
                new LatLng(27.712415, -97.324221),
                new LatLng(27.712596, -97.323969),
                new LatLng(27.712482, -97.323800),
                new LatLng(27.711862, -97.324266),
                new LatLng(27.712302, -97.324779)
        ));
        buildingSet.put(35, new Building("Driftwood", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712567, -97.321647),
                new LatLng(27.712409, -97.321571),
                new LatLng(27.712351, -97.321744),
                new LatLng(27.712505, -97.321822)
        ));
        buildingSet.put(36, new Building("Sandpiper", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712389, -97.321509),
                new LatLng(27.712448, -97.321335),
                new LatLng(27.712289, -97.321261),
                new LatLng(27.712229, -97.321438)
        ));
        buildingSet.put(37, new Building("Hammerhead Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712466, -97.320112),
                new LatLng(27.712328, -97.319312),
                new LatLng(27.713277, -97.318809),
                new LatLng(27.713565, -97.319685)
        ));
        buildingSet.put(38, new Building("Sanddollar Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713663, -97.322349),
                new LatLng(27.714402, -97.321829),
                new LatLng(27.713818, -97.320285),
                new LatLng(27.713054, -97.320652)
        ));
        buildingSet.put(39, new Building("Seabreeze Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.713388, -97.322629),
                new LatLng(27.713049, -97.322804),
                new LatLng(27.712798, -97.322147),
                new LatLng(27.713233, -97.321905)
        ));
        buildingSet.put(40, new Building("Tarpon Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712839, -97.321913),
                new LatLng(27.712359, -97.320714),
                new LatLng(27.712799, -97.320508),
                new LatLng(27.713263, -97.321686)
        ));
        buildingSet.put(41, new Building("Curlew Parking Lot", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.712823, -97.323374),
                new LatLng(27.712371, -97.322184),
                new LatLng(27.711798, -97.322369),
                new LatLng(27.712222, -97.323639)
        ));
        buildingSet.put(42, new Building("Performing Arts Ceter", "6300 Ocean Dr, Corpus Christi, TX 78412",
                new LatLng(27.714691, -97.322816),
                new LatLng(27.714416, -97.322075),
                new LatLng(27.714071, -97.322230),
                new LatLng(27.714310, -97.322892)
        ));


    }

    public class Building {

        private String buildingName;
        private LatLng[] points;
        private String address;

        public Building(String buildingName, String address, LatLng... points) {
            this.buildingName = buildingName;
            this.points = points;
            this.address = address;
            this.drawPloygon();
        }

        public String getBuildingName() {
            return buildingName;
        }

        public LatLng[] getPoints() {
            return points;
        }

        public String getAddress() {
            return address;
        }

        private void drawPloygon() {

            PolygonOptions po = new PolygonOptions();
            for (LatLng point : this.points) {
                po.add(point);
            }

            //if ()
            po.strokeWidth(3).strokeColor(Color.rgb(105, 105, 105)).fillColor(Color.rgb(240, 165, 20));
            map.addPolygon(po);
        }

    }
    // -----------------------------------------------------------
}