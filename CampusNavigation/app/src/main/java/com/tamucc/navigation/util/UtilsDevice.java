package com.tamucc.navigation.util;


import android.content.Context;
import android.util.DisplayMetrics;

public class UtilsDevice
{

    public static int getScreenWidth(Context context)
    {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        return metrics.widthPixels;
    }
}