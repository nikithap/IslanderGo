package com.tamucc.navigation.model;




public class Info {

	private int id;


	private String information;


	private int buildingname;
	
	
	private String buildingName;



	private String building;

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public int getBuildingname() {
		return buildingname;
	}

	public void setBuildingname(int buildingname) {
		this.buildingname = buildingname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

}
