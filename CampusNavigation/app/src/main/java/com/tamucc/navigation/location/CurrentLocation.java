package com.tamucc.navigation.location;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tamucc.navigation.MapsActivity;
import com.tamucc.navigation.database.DB_Helper;
import com.tamucc.navigation.database.DB_Operations;
import com.tamucc.navigation.database.DB_Route;
import com.tamucc.navigation.direction.Route;
import com.tamucc.navigation.recordings.RecordOperations;
import com.tamucc.navigation.geometry.TargetBuilding;
import com.tamucc.navigation.geometry.GoogleLatLngDistance;
import com.tamucc.navigation.mapdrawing.BuildingDrawing;
import com.tamucc.navigation.mapdrawing.BuildingDrawing.Building;
import com.tamucc.navigation.recordings.SaveRecordingTask;
import com.tamucc.navigation.routefilter.Location_Tamucc;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

//This class will be execute in Async task
public class CurrentLocation implements Runnable {

    private final static int GPS_LOST_TIME = 4000;
    private final static int GPS_LOST_COUNTER = 13;

    private Timer timer1;
    private LocationManager lm;
    private LocationResult locationResult;
    private boolean gps_enabled = false;
    private boolean gps_lost_flag = false;
    private boolean gps_indoor_recording_flag = false;
    private boolean network_enabled = false;
    private boolean timer_cancelled = false;
    private MapsActivity mContext;
    private final static int TIME_FOR_GPS_WHEN_NO_NETWORK = 120000;
    private Route rr;
    private Thread mythread = null;
    private RecordOperations fo;

    private Location MyLastLocation;
    private Long mLastLocationMillis;
    private HaoGPSListener myGpsListener;
    private BuildingDrawing bd;
    private int counter = 0;
    private String destination;
    private LatLng enteredBuildingLatLng;

    public void setDestination(String des) {
        this.destination = des;
    }

    // abstract class for call back
    public static abstract class LocationResult {
        public abstract void gotLocation(Location location);
    }

    /**
     * Constructor
     *
     * @param homeActivity
     * @param map
     * @param bd
     */
    GoogleMap map1;
    public CurrentLocation(MapsActivity homeActivity, GoogleMap map,
                           BuildingDrawing bd) {
        this.mContext = homeActivity;
         map1 = map;
        this.bd = bd;
        fo = new RecordOperations();
        rr = new Route(fo);
        this.myGpsListener = new HaoGPSListener();


    }

    public void beginRoute() {
        //  rr.toggleRecordState();

        rr.startRecord();
        gps_lost_flag = false;
        fo.fileInitialization("txt");
        startThread();
    }

    private void startThread() {
        mythread = new Thread(this); // connect to and start the run method
        mythread.start();
    }


    public String disableLocationUpdate(CurrentLocationAsync locationTask) {

        String returnName = fo.getFileName();


        if (rr.recordHasStarted()) {

            rr.toggleRecordState();

            Toast.makeText(mContext, "Recording data saving in " + returnName,
                    Toast.LENGTH_SHORT).show();

            DB_Helper dbh = new DB_Helper();
            LatLng to = dbh.getCenterPointOfABuildingFromDB(destination);
            Location_Tamucc lh = new Location_Tamucc(to.latitude,
                    to.longitude, System.currentTimeMillis());
            rr.bufferStore(lh);


            rr.checkRemainingElementsInBQandCloseBuffer(fo);

            if (mythread != null) {
                mythread.interrupt();
            }

            fo.processRecord_kalman_filter("a", true);

            for (int i = 0; i < 1; i++)
                fo.processRecord_kalman_filter("a", false);

            fo.processRecord_delete_outliers("a", true);
            DB_Route returnRoute = fo
                    .calculate_distance_time_andGet_StartEndLatLng();

            if (returnRoute == null) { // invalid route,too short

            } else {

                returnRoute.setFileName(fo.getProcessedFileName());

                returnRoute.setDestination(destination);

                SaveRecordingTask uploadTask = new SaveRecordingTask(returnRoute, mContext);
                uploadTask.execute();

                DB_Operations op = new DB_Operations();
                op.open();
                op.insertARoute(returnRoute, true);// need to add more attributes
                op.close();
            }

            // counter for gps signal
            counter = 0;

            // iterrupt


            locationTask.cancel(true);
        }
        return returnName;
    }

    public LatLng getEnteredBuildingLatLng() {
        if (gps_lost_flag) {
            return enteredBuildingLatLng;
        } else {
            return null;
        }
    }


    private class HaoGPSListener implements GpsStatus.Listener {

        @Override
        public void onGpsStatusChanged(int event) {

            System.out.println("GPS EVENT" + GpsStatus.GPS_EVENT_SATELLITE_STATUS);
            switch (event) {
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    boolean isGPSFix;
                    if (MyLastLocation != null) {
                        isGPSFix = (SystemClock.elapsedRealtime() - mLastLocationMillis) < GPS_LOST_TIME;
                    } else {
                        return;
                        // MyLastLocation = getMyLastLocation();
                    }

                    if (isGPSFix) { // A fix has been acquired.
                        System.out.println("GPS running and counter:" + counter);

                        // counter>=10, meaning signal comes back
                        if (counter >= GPS_LOST_COUNTER) {

                            // reset counter
                            counter = 0;

                            // reset so that if signal lost again,can check which
                            // building we entered
                            gps_lost_flag = false;

                            // check can keep on recording
                            if (gps_indoor_recording_flag == false) {

                                gps_indoor_recording_flag = true;

                                Toast.makeText(mContext, "GPS signal back",
                                        Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else { // lost signal for consecutive 4 seconds
                        System.out.println("GPS lost");

                        counter = 0;

                        if (!gps_lost_flag) { // just one time

//                            Toast.makeText(mContext, "GPS signal lost",
//                                    Toast.LENGTH_SHORT).show();

                            gps_lost_flag = true;

                            // set the flag so that no more recording
                            gps_indoor_recording_flag = false;

                            // judge which building I go, return a building obj
                            TargetBuilding ewb = new TargetBuilding(
                                    mContext, MyLastLocation, bd);
                            Building closestBuilding = ewb
                                    .getWhichBuildingEntered();
                            // get the building LatLng
                            LatLng enteredBLL = ewb.getEnteredBuildingLatLng();

                            //for routeOptimization
                            enteredBuildingLatLng = enteredBLL;

                            GoogleLatLngDistance glld = new GoogleLatLngDistance();
                            double estimateDistance = glld.GetDistance(
                                    enteredBLL.latitude, enteredBLL.longitude,
                                    MyLastLocation.getLatitude(),
                                    MyLastLocation.getLongitude());
                            if (estimateDistance <= 150) {
                                Toast.makeText(
                                        mContext,
                                        "You entered "
                                                + closestBuilding.getBuildingName(),
                                        Toast.LENGTH_LONG).show();

                                System.out.println("Entered Building:  "
                                        + enteredBLL);

                                // add that center point and add into the route
                                if (rr.recordHasStarted()) {
                                    long ctime = System.currentTimeMillis();
                                    Location_Tamucc lh = new Location_Tamucc(
                                            enteredBLL.latitude,
                                            enteredBLL.longitude, ctime);

                                    // can I fix that?
                                    //rr.bufferStore(lh);


                                }
                            } else {
                                Toast.makeText(
                                        mContext,
                                        "Your nearest building is "
                                                + closestBuilding.getBuildingName(),
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX: // execute only once


                    gps_indoor_recording_flag = true;
                    gps_lost_flag = false;
                    counter = 0;


                    System.out.println("****First Time");

                    isGPSFix = true;

                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                    System.out.println("****GPS stop");
                    break;
            }

        }

    }

    /**
     * Shared method for location listeners
     *
     * @param location
     */
    private void checkTimerAndRoute(Location location) {

        if (!timer_cancelled) {
            timer_cancelled = true; // no more execute this if
            timer1.cancel();

        }

        // update my location to map_activity
        locationResult.gotLocation(location);

        if (location != null) {
            mLastLocationMillis = SystemClock.elapsedRealtime();

            // update my location to this class
            MyLastLocation = location;

            // increment my signal counter
            counter++;
        } else {
            return;
        }

        System.out.println("CHANGED LOCATION" + (rr.recordHasStarted() + "  " + gps_indoor_recording_flag));
        Building closestBuilding;
        TargetBuilding ewb = new TargetBuilding(
                mContext, MyLastLocation, bd);
        closestBuilding = ewb
                .getWhichBuildingEntered();

        if (closestBuilding != null) {
            mContext.pickUp.setText(closestBuilding.getBuildingName());
        }
        // if a record has been started
        if (rr.recordHasStarted()) {
            Location_Tamucc lh = new Location_Tamucc(location.getLatitude(),
                    location.getLongitude(), location.getTime());
            rr.bufferStore(lh);


            LatLng enteredBLL = ewb.getEnteredBuildingLatLng();
            enteredBuildingLatLng = enteredBLL;

            GoogleLatLngDistance glld = new GoogleLatLngDistance();
            double estimateDistance = glld.GetDistance(
                    enteredBLL.latitude, enteredBLL.longitude,
                    MyLastLocation.getLatitude(),
                    MyLastLocation.getLongitude());
            if (estimateDistance <= 150) {
                Toast.makeText(
                        mContext,
                        "You entered "
                                + closestBuilding.getBuildingName(),
                        Toast.LENGTH_LONG).show();

                System.out.println("Entered Building:  "
                        + enteredBLL);


            } else {
                Toast.makeText(
                        mContext,
                        "Your nearest building is "
                                + closestBuilding.getBuildingName(),
                        Toast.LENGTH_LONG).show();
            }

        }

        if (oldLocation != null) {
            double bearing = angleFromCoordinate(oldLocation.getLatitude(), oldLocation.getLongitude(), location.getLatitude(), location.getLongitude());
            locationUpdate(location, bearing);

            changeMarkerPosition(bearing);
        }
        oldLocation = location;
    }

    /**
     * define GPS and Network listeners
     */
    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            checkTimerAndRoute(location);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 12;

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            checkTimerAndRoute(location);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };


    @Override
    public void run() {
        while (true) {
            System.out.println("Recording started");
            rr.bufferTakeAndAddToFile();
        }
    }


    public boolean setupLocation(Context context, LocationResult result) {

        locationResult = result;
        if (lm == null)
            lm = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {

        }

        System.out.println("GPS EN" + gps_enabled + " NETWORK " + network_enabled);
        // don't start listeners if no provider is enabled
        if (!gps_enabled && !network_enabled)
            return false;


        // setup listeners
        if (gps_enabled) {

            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 2,
                    locationListenerGps);
        }

        if (network_enabled) {
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 2,
                    locationListenerNetwork);
        }

        // add the gps listener
        lm.addGpsStatusListener(myGpsListener);

        timer1 = new Timer();
        timer1.schedule(new RetrieveLastLocation(),
                TIME_FOR_GPS_WHEN_NO_NETWORK);
        return true;
    }


    public Location getMyLastLocation() {
        return MyLastLocation;
    }


    class RetrieveLastLocation extends TimerTask { // runnable,it's a thread
        @Override
        public void run() {
            try {
                lm.removeUpdates(locationListenerGps);
                lm.removeUpdates(locationListenerNetwork);

                Location net_loc = null, gps_loc = null;
                if (gps_enabled) {
                    gps_loc = lm
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                if (network_enabled) {
                    net_loc = lm
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                if (gps_loc != null && net_loc != null) {
                    if (gps_loc.getTime() > net_loc.getTime()) {
                        locationResult.gotLocation(gps_loc);
                    } else {
                        locationResult.gotLocation(net_loc);
                    }
                    return;
                }
                if (gps_loc != null) {
                    locationResult.gotLocation(gps_loc);
                    return;
                }
                if (net_loc != null) {
                    locationResult.gotLocation(net_loc);
                    return;
                }
                locationResult.gotLocation(null);
                return;
            } catch (Exception e) {
                Toast.makeText(mContext, "Can't get any signals",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static double angleFromCoordinate(double lat1, double long1, double lat2,
                                       double long2) {
        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(x, y);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        brng = 360 - brng;
        return brng;
    }

    private void locationUpdate(Location location, double angle) {
        LatLng latLng = new LatLng((location.getLatitude()), (location.getLongitude()));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(location.getLatitude(), location.getLongitude()))
                .zoom(17)
                .bearing((float) angle)
                .tilt(30)
                .build();
        map1.animateCamera(CameraUpdateFactory.newCameraPosition(position));
    }

    private static float angle;
    private static Location oldLocation;

    private void changeMarkerPosition(double position) {
        float direction = (float) position;

        if (direction == 360.0) {
            //default
            // marker.setRotation(angle);
        } else {
            // marker.setRotation(direction);
            angle = direction;
        }
    }

    public void removeLMUpdate() {
        lm.removeUpdates(locationListenerGps);
        lm.removeUpdates(locationListenerNetwork);
        lm.removeGpsStatusListener(myGpsListener);
    }
}