package com.tamucc.navigation.database;

public class DB_Building {

	private int Bid;
	private String BuildingName;
	private String BuildingAddress;
	private int BuildingIcon;
	private double Location_lat;
	private double Location_lng;
	private int QueryTime;


	public DB_Building(String BuildingName, int QueryTime,int BuildingIcon){
		this.BuildingName = BuildingName;
		this.QueryTime = QueryTime;
		this.BuildingIcon = BuildingIcon;
	}
	

	
	public String getBuildingName(){
		return this.BuildingName;
	}
	
	public int getBuildingIcon(){
		return this.BuildingIcon;
	}
	
	public int getQueryTime(){
		return this.QueryTime;
	}
	
	public void setQueryTime(int qt){
		this.QueryTime = qt;
	}
	
	public void incrementQueryTime(){
		this.QueryTime++;
	}
	
	@Override
	public String toString(){
		return this.BuildingName + " and " + this.QueryTime + " times";
		
	}
	
}
